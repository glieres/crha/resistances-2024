
.. _crha_2024_02_21:

===============================================================================
2024-02-21 **Annonce du Rassemblement des Glières des 10, 11 et 12 mai 2024**
===============================================================================


Annonce
========

Bonjour,

Le Rassemblement des Glières 2024 aura lieu le week-end de l’ascension, soit
les 10, 11 et 12 mai.

L’accès est totalement gratuit, des chapeaux seront présents par çi par là si
vous souhaitez nous soutenir.

Vous pouvez aussi le faire par l’intermédiaire d’HelloAsso :
https://www.helloasso.com/associations/citoyens-resistants-d-hier-et-d-aujourd-hui/formulaires/1/widget

Vendredi 10 mai 2024
=========================

Le rassemblement débutera le vendredi soir avec la pièce de théâtre
"Discours de la servitude volontaire" d’Etienne de la Boétie, traduit en
langage moderne par la  Compagnie **Avec vue sur la mer**.

Samedi 11 mai 2024
=========================

Le samedi, ce sont les conférences, les films, les expositions, la librairie,
le village des associations, le soleil, le ciel bleu…

Pour les conférences, des ébauches (résumées) de titres, donc qui peuvent
évoluer:

- Dans quel monde vivons nous, regards géopolitiques et actions
  politiques ?,
- "Libertés publiques et désobéissance civile",
- "Nourrir le monde",
- "Rendez nous l’espoir",
- "Quel état hier, aujourd’hui, demain ?",
- "Ecologie politique, pourquoi, comment ?",
- "Le sol, accaparement et destruction, que faire ?"


Le samedi se termine par un concert de **Les Goguettes**

Dimanche 12 mai 2024
=========================

Le dimanche matin, ce sont les Paroles de résistances au Plateau des Glières.

La suite du programme sera mis à jour au fil de l’eau sur notre site internet.
