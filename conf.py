# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config
# -- Path setup --------------------------------------------------------------
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import platform
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx
import sphinx_material

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
# sys.path.insert(0, os.path.abspath("./src"))

project = "CRHA 2024"
html_title = project

author = f"Human people"
release = "0.1.0"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"
today = version

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.extlinks",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx_copybutton",
]
autosummary_generate = True
autoclass_content = "class"

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", ".venv"]
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
extensions.append("sphinx_material")
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"

extensions.append("sphinx.ext.intersphinx")
intersphinx_mapping = {
    "liens_iran": ("https://iran.frama.io/linkertree/", None),
    "iran_luttes": ("https://iran.frama.io/luttes/", None),
    "iran_2024": ("https://iran.frama.io/luttes-2024/", None),
    "grenoble_retraites": ("https://grenoble.frama.io/luttes/retraites/", None),
    "crha": ("https://glieres.frama.io/crha/resistances/", None),
    "crha_2023": ("https://glieres.frama.io/crha/resistances-2023/", None),
    "antisem": ("https://luttes.frama.io/contre/l-antisemitisme/", None),
    "raar_2024": ("https://antiracisme.frama.io/infos-2024", None),
    "raar_2023": ("https://antiracisme.frama.io/infos-2023", None),
}
extensions.append("sphinx.ext.todo")
todo_include_todos = True


# material theme options (see theme.conf for more information)
# https://gitlab.com/bashtage/sphinx-material/blob/master/sphinx_material/sphinx_material/theme.conf
# Colors
# The theme color for mobile browsers. Hex color.
# theme_color = #3f51b5
# Primary colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange,
# brown, grey, blue-grey, white
# Accent colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange
# color_accent = blue
# color_primary = blue-grey

# material theme options (see theme.conf for more information)
html_theme_options = {
    "base_url": "https://glieres.frama.io/crha/resistances-2024/",
    "repo_url": "https://framagit.org/glieres/crha/resistances-2024",
    "repo_name": project,
    "html_minify": False,
    "html_prettify": True,
    "css_minify": True,
    "repo_type": "gitlab",
    "globaltoc_depth": -1,
    "color_primary": "black",
    "color_accent": "cyan",
    "theme_color": "#2196f3",
    "nav_title": f"{project} ({today})",
    "master_doc": False,
    "nav_links": [
        {
            "href": "genindex",
            "internal": True,
            "title": "Index",
        },
        {
            "href": "http://www.citoyens-resistants.fr/",
            "internal": False,
            "title": "CRHA",
        },
        {
            "href": "http://glieres.frama.io/crha/linkertree",
            "internal": False,
            "title": "Liens CRHA",
        },
        {
            "href": "https://www.youtube.com/@CRHA_RassemblementdesGlieres/videos",
            "internal": False,
            "title": "Vidéos CRHA",
        },
    ],
    "heroes": {
        "index": "CRHA 2024",
    },
    "table_classes": ["plain"],
}
# https://github.com/sphinx-contrib/yasfb
extensions.append("yasfb")
feed_base_url = html_theme_options["base_url"]
feed_author = "Scribe"
# https://sphinx-design.readthedocs.io/en/furo-theme/get_started.html
extensions.append("sphinx_design")
# https://sphinx-tags.readthedocs.io/en/latest/quickstart.html#installation
extensions.append("sphinx_tags")
tags_create_tags = True
# Whether to display tags using sphinx-design badges.
tags_create_badges = True
extensions += ["sphinxcontrib.youtube"]


language = "en"
html_last_updated_fmt = ""

todo_include_todos = True

html_use_index = True
html_domain_indices = True

copyright = f"-2500-{now.year}, {author} Built with sphinx {sphinx.__version__} Python {platform.python_version()}"

rst_prolog = """
.. |crha| image:: /images/crha_avatar.png
.. |important| image:: /images/important_ici.png
.. |rebellion| image:: /images/extinction_rebellion.png
.. |medics| image:: /images/street_medics_avatar.png
.. |iran_solidarite| image:: /images/iran_solidarites_avatar.webp
.. |JinaAmini| image:: /images/mahsa_jina_amini_avatar.png
.. |CecileKohler| image:: /images/cecile_kohler_avatar.png
.. |JacquesParis| image:: /images/jacques_paris_avatar.png
.. |LouisArnaud| image:: /images/louis_arnaud_avatar.png
.. |toomaj| image:: /images/toomaj_salehi_avatar.webp
.. |FluxWeb| image:: /images/rss_avatar.webp
"""

