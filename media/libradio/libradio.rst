.. index::
   pair: Media ; libradio

.. _libradio:

=========================================
**libradio**
=========================================

- https://libradio.org/presentation/libradio/
- info[AT] libradio.org


Auteur Piotr
----------------

- https://libradio.org/author/piotr/


Voici 18 ans que les `Citoyen-nes Resistant-es d’Hier et d’Aujourdhui (CRHA) <https://citoyens-resistants.fr/>`_
organisent le rassemblement des Glières pour tirer des liens entre le passé et
le présent dans ce haut lieu de la résistance aux fascistes.

En mars 1944 plus de 120 maquisards venus de tous bords ont perdu la vie dans
ces montagnes mais je vous raconterais leur histoire une autre fois dans un CLS.

Je ne sais trop pourquoi mais c’était une première pour Libradio qui a été
accueilli très chaleureusement par les organisateurs et les bénévoles qui font
un travail extraordinaire !!

Le programme était passionnant mais n’ayant pas encore le don d’ubiquité
nous avons du choisir.

Bonne écoute !


11-12-mai-2024-rassemblement-des-glieres-2024 par Piotr
===============================================================================================

- https://libradio.org/11-12-mai-2024-rassemblement-des-glieres-2024/
