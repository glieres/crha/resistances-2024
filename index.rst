
.. raw:: html

   <a rel="me" href="https://kolektiva.social/@grenobleluttes"></a>
   <a rel="me" href="https://kolektiva.social/@raar"></a>
   <a rel="me" href="https://framapiaf.org/@goldman_bakounine"></a>
   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://piaille.fr/@ldh_grenoble"></a>


.. ♀️✊ ⚖️ 📣
.. ✊🏻✊🏼✊🏽✊🏾✊🏿
.. 🤥
.. 🤪
.. ⚖️ 👨‍🎓
.. 🔥
.. 💧
.. ☠️
.. ☣️
.. 🪧
.. 🇺🇳
.. 🇫🇷
.. 🇧🇪
.. 🇺🇦
.. 🇬🇧
.. 🇨🇭
.. 🇩🇪
.. 🌍 ♀️✊🏽
.. 🇮🇷
.. 🎥 🎦
.. 🎇 🎉

|FluxWeb| `RSS <https://glieres.frama.io/crha/resistances-2024/rss.xml>`_

.. un·e

.. figure:: images/crha_logo.png
   :align: center
   :width: 200


.. figure:: images/affiche_2024.webp

.. _glieres_crha_2024:

============================================================
**Rassemblement des Glières des 10, 11 et 12 mai 2024**
============================================================

- http://www.citoyens-resistants.fr
- http://www.citoyens-resistants.fr/Documents/Programme2024.pdf
- https://www.youtube.com/@CRHA_RassemblementdesGlieres/videos
- https://fr-fr.facebook.com/rassemblementdesglieres/



::

    129 rue des Fleuries , Thorens-Glières, France
    citoyen.2008@yahoo.fr


.. warning::
   Vous pouvez nous soutenir en bénéficiant d’un don défiscalisé en utilisant
   notamment `Helloasso https://www.helloasso.com/associations/citoyens-resistants-d-hier-et-d-aujourd-hui/formulaires/1/widget <https://www.helloasso.com/associations/citoyens-resistants-d-hier-et-d-aujourd-hui/formulaires/1/widget>`_

.. figure:: images/carte_thorens_glieres.png
   :align: center

   Thorens-Glières, cinéma Le Parnal, monument en mémoire des résistants espagnols, MJC et salle Tom Morel https://www.openstreetmap.org/#map=18/45.99726/6.24842


.. youtube:: ZV0wccec1tE

.. toctree::
   :maxdepth: 5

   05/05
   02/02
   videos/videos
   media/media
