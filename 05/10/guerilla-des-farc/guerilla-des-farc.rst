

.. _farc_2024_05_10:

==========================================================================================================
2024-05-10  🎥 **Guérilla des FARC** de Pierre Carles, en avant-première au cinéma Le Parnal **à 20h**
==========================================================================================================

- http://www.youtube.com/watch?v=sfZuaNMjtio
- https://fr.wikipedia.org/wiki/Forces_arm%C3%A9es_r%C3%A9volutionnaires_de_Colombie



.. youtube:: sfZuaNMjtio

Présentation
===============

.. figure:: images/affiche.webp

Les FARC, la plus ancienne guérilla de la planète, va signer un accord de paix.

En 1965, Jean-Pierre Sergent et Bruno Muel furent les premiers à filmer les
rebelles dans un pays ravagé par les inégalités et la violence des propriétaires
terriens. (...)

Ils filment la vie quotidienne des paysans-guérilleros.

Manuel Marulanda, qui dirigera pendant un demi-siècle le plus important maquis
communiste de la seconde moitié du XXe siècle, accorde sa première interview.

Personne ne sait alors qu'il deviendra une figure mythique, au même titre
que Che Guevara. Muel, Sergent et les maquisards, les survivants de cette époque
et les plus jeunes, reviennent sur cette résistance hors-normes.


Description Wikipedia
=======================

Les Forces armées révolutionnaires de Colombie –
Armée du peuple (espagnol : Fuerzas armadas revolucionarias de Colombia –
Ejército del Pueblo), généralement appelées FARC, le sigle exact étant FARC-EP,
étaient la principale guérilla communiste impliquée dans le conflit armé colombien.

L'organisation est placée sur la liste officielle des organisations terroristes
des États-Unis à la suite des attentats du 11 septembre 2001, du Canada ,
de la Nouvelle-Zélande, et pour l'Union européenne.

Pour les Nations unies, les guérillas colombiennes seraient responsables
de 12 % des assassinats de civils perpétrés dans le cadre du conflit armé,
les paramilitaires de 80 % et les forces gouvernementales des 8 % restant5.

Après quatre années de négociations, leurs représentants signent le 26
septembre 2016 un accord de paix avec le gouvernement visant à mettre en œuvre
leur démobilisation définitive.

À la suite de cet accord, les FARC fondent le 31 août 2017 un parti politique
légal sous le même acronyme, appelé Force alternative révolutionnaire commune
(Fuerza Alternativa Revolucionaria del Común, FARC).
