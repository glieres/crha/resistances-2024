.. index::
   ! Discours de la servitude volontaire** salle Tom Morel à 20h
   ! Étienne de La Boétie

.. _servitude_2024_05_10:

===============================================================================
2024-05-10 **Discours de la servitude volontaire** salle Tom Morel **à 20h**
===============================================================================

- http://www.citoyens-resistants.fr/Documents/Programme2024.pdf
- https://www.citoyens-resistants.fr/Documents/Boetie.pdf
- https://fr.wikipedia.org/wiki/%C3%89tienne_de_La_Bo%C3%A9tie
- https://fr.wikipedia.org/wiki/Discours_de_la_servitude_volontaire

.. figure:: images/annonce.webp

.. figure:: images/affiche.webp

   https://www.citoyens-resistants.fr/Documents/Boetie.pdf


Discours de la servitude volontaire par Cie AVEC VUE SUR LA MER
====================================================================

Vers 1550, un jeune homme de 17 ans, Etienne de La Boëtie, écrit un texte
lumineux qui sera salué de siècle en siècle, de Montaigne à … Michel Onf...

Qu’est-ce qui fait qu’un peuple tout entier se laisse asservir ?
Et que doit-il faire, ce peuple, pour recouvrer sa liberté ?

(...) La Boëtie questionne les concepts de liberté, d’égalité et de… fraternité.

Il explore les mécanismes de la tyrannie bien sûr mais surtout notre rapport
ambigu au pouvoir et à la soumission.

En humaniste, sociologue, psychologue des masses avant l’heure, sans donner
de leçons, il met de la pensée en mouvement et surtout nous invite à le faire
avec lui.


Étienne de La Boétie
========================

- https://fr.wikipedia.org/wiki/%C3%89tienne_de_La_Bo%C3%A9tie
- https://fr.wikipedia.org/wiki/Discours_de_la_servitude_volontaire

Étienne de La Boétie ([labɔesi]1, parfois [labwa'ti]) est un écrivain humaniste,
un poète et un juriste français né le 1er novembre 1530 à Sarlat, ville du
sud-est du Périgord, et mort le 18 août 1563 à Germignan, dans la commune
du Taillan-Médoc, près de Bordeaux.

La Boétie est célèbre pour son `Discours de la servitude volontaire <https://fr.wikipedia.org/wiki/Discours_de_la_servitude_volontaire>`_.

À partir de 1558, il est l’ami intime de Montaigne, qui lui rend un hommage
posthume dans ses Essais4.

Discours de la servitude volontaire
=======================================

- https://fr.wikipedia.org/wiki/Discours_de_la_servitude_volontaire

Le **Discours de la servitude volontaire** ou le **Contr'un** est un ouvrage rédigé par
Étienne de La Boétie.
Publié en latin, par fragments en 1574, puis intégralement en français en 1576,
il a été écrit par La Boétie probablement **à l'âge de 16 ou 18 ans**.

Ce texte consiste en un court réquisitoire contre la tyrannie qui étonne par
son érudition et par sa profondeur, alors qu'il a été rédigé par un jeune
homme.
Ce texte pose la question de la légitimité de toute autorité sur une population
et essaie d'analyser les raisons de la soumission de celle-ci (rapport
« domination-servitude »).

L’originalité de la thèse de La Boétie est de soutenir que, contrairement
à ce que beaucoup croient, la servitude n'est pas imposée par la force mais
volontaire.
Si ce n'était pas le cas, comment concevoir qu’un petit nombre contraigne
l’ensemble des autres citoyens à obéir aussi servilement ?

En fait, tout pouvoir, même quand il s’impose d’abord par la force des armes,
ne peut dominer et exploiter durablement une société sans la collaboration,
active ou résignée, d’une partie notable de ses membres.

Pour La Boétie, **Soyez donc résolus à ne plus servir et vous serez libres**

