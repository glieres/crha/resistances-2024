.. index::
   ! Prises de parole au plateau des Glières

.. _crha_2024_05_12:

==================================================================================
|crha| Dimanche 12 mai 2024 10h30 **Prises de parole au plateau des Glières**
==================================================================================

- https://www.youtube.com/@CRHA_RassemblementdesGlieres/videos
- https://fr.wikipedia.org/wiki/Plateau_des_Gli%C3%A8res
- https://www.geoportail.gouv.fr/carte?c=6.329477350590899,45.96409759654725&z=17&l0=GEOGRAPHICALGRIDSYSTEMS.MAPS::GEOPORTAIL:OGC:WMTS(1)&l1=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2::GEOPORTAIL:OGC:WMTS(1)&l2=ORTHOIMAGERY.ORTHOPHOTOS::GEOPORTAIL:OGC:WMTS(1)&permalink=yes
- https://app.webcam-hd.com/conseil-general-74/cg74_plateau-des-glieres

.. figure:: images/affiche_militantes.webp

   http://www.citoyens-resistants.fr/Documents/Programme2024.pdf


.. youtube:: ZV0wccec1tE


.. warning::
    Les Paroles de Résistances sont un hommage aux résistant.e.s d'hier et
    d'aujourd'hui, que nous venons écouter avec respect, sans aucun signe
    qui nous distingue les uns des autres.

    Pour cela, pas de banderole, drapeau, slogan, ni aucun autre symbole qui
    nous identifie et nous sépare.

.. warning:: Toutes les interventions sont traduites en langue des signes LSF

.. figure:: ../images/monument_des_glieres.png
   :align: center
   :width: 500

   https://fr.wikipedia.org/wiki/Monument_national_%C3%A0_la_R%C3%A9sistance_du_plateau_des_Gli%C3%A8res


.. raw:: html

   <iframe width="800" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" sandbox="allow-forms allow-scripts allow-same-origin"
       src="https://www.geoportail.gouv.fr/embed/visu.html?c=6.329477350590899,45.96409759654725&z=17&l0=GEOGRAPHICALGRIDSYSTEMS.MAPS::GEOPORTAIL:OGC:WMTS(1)&l1=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2::GEOPORTAIL:OGC:WMTS(1)&l2=ORTHOIMAGERY.ORTHOPHOTOS::GEOPORTAIL:OGC:WMTS(1)&permalink=yes" allowfullscreen>
   </iframe>




.. toctree::
   :maxdepth: 3

   celine-verzeletti/celine-verzeletti
   iyad-allastal/iyad-allastal
   thomas-brail/thomas-brail
   genevieve-legay/genevieve-legay
   hommage-charles-piaget/hommage-charles-piaget
   melanie-berger-volle/melanie-berger-volle
   gerard-fumex/gerard-fumex
   giorgos-kalaitzidis/giorgos-kalaitzidis
