.. index::
   ! Mélanie Beger-Volle

.. _melanie_beger_volle:

==========================================================================================================
**Mélanie Beger-Volle** née le 8 octobre 1921, autrichienne résistante en France, lecture de son texte
==========================================================================================================

- https://fr.wikipedia.org/wiki/M%C3%A9lanie_Berger-Volle

.. figure:: images/portrait.webp
   :width: 400

.. figure:: images/portrait_avec_livres.webp


Introduction
=================

Mélanie Berger-Volle, née le 8 octobre 1921 à Vienne (Autriche). Franco-autrichienne,
couturière, elle fut militante trotskiste contre l'austrofascisme autrichien
et résistante française. Elle est aussi témoin de son temps.


Biographie
============

Mélanie Berger est née dans une famille juive à Vienne-Leopoldstadt.

Après avoir achevé sa scolarité au collège, elle a appris le métier de corsetière
dans le cadre de son apprentissage de couturière.

Elle dit avoir développé une pensée politique à partir de l'âge 13 ans.

Socialisée politiquement dans le mouvement ouvrier, elle a, dès l'âge de 15 ans,
participé à des activités illégales pendant l'austrofascisme.

Elle a adhéré pendant sa scolarité au parti des Socialistes révolutionnaires
d'Autriche (RSÖ).
Les membres de ce parti interdit se retrouvaient sur une plage naturiste de
la Lobau.
Elle raconte dans une interview que c'est là qu'elle a commencé, nue, ses
premières discussions politiques.
Elle adhéra peu après à l'organisation internationaliste et antistalinienne
des Communistes révolutionnaires d'Autriche (RKÖ).

En mai 1938, **menacée d'emprisonnement en tant que juive** et communiste après
l'Anschluss, elle traversa l'Allemagne en autostop1 pour se rendre en Belgique,
à Anvers où elle séjourna illégalement.

Elle passa en France avec des amis début 1939 parmi les travailleurs frontaliers,
vêtue en homme. A Paris, elle fut tout d'abord protégée par son nom aussi
français qu'allemand et obtint même une autorisation de séjour convoitée.

Au début de la Seconde Guerre mondiale, elle fut toutefois poursuivie comme
les autres antifascistes en tant qu'ennemie étrangère et transférée à Clermont-Ferrand.
Elle devait partir de là en train pour être internée au camp de Gurs.
Mais elle réussit à échapper à l'incarcération et trouva un travail comme
bonne chez un avocat. Elle put ainsi échapper provisoirement à la détention.

Tandis que ses amis étaient détenus aux Milles, elle réussit à maintenir les
liens du groupe RKÖ avec ses bases à Anvers, Les Milles, Londres, Zürich et
New York.
Pour échapper à la Wehrmacht, elle se rendit dans le sud de la France à Montauban.
Elle distribua aux soldats allemands des tracts anti-hitlériens.

Le 26 janvier 1942, elle fut arrêtée par la police de Toulouse au 3 quai
Docteur-Laffargue et conduite au commissariat de Montauban où elle fut interrogée,
battue et ensuite transférée a la prison Saint-Michel à Toulouse.

**Le 18 décembre 1942, elle fut condamnée par la cour d'appel de Toulouse à 15 ans
de travaux forcés et à 20 ans d'interdiction de séjour pour "activités communistes
et anarchistes" et pour diffusion, dans un but de propagande, de tracts de
nature à nuire à l’intérêt national**.

Après son procès, elle est transférée à la prison des Baumettes à Marseille,
où elle sera incarcérée avec trois autres femmes, Jeanne Katzenstein,
Madeleine Goetzmann et Margot Usclat. La situation d'"Anna" ou de "Nelly",
les pseudonymes de Mélanie Berger dans la Résistance, devint dangereuse car
la Gestapo recherchait depuis 1943 les prisonniers politiques dans les prisons
du régime de Vichy.

Après avoir été transférée en octobre 1943 à l'hôpital-prison pour être soignée
d'une jaunisse aigüe, elle put être libérée lors d'une action spectaculaire
par des membres de son groupe auxquels s'était joint un soldat en uniforme
de la Wehrmacht qui voulait déserter et qui avait pu être convaincu de
participer à cette action.

Ils vinrent la chercher pour un interrogatoire avec de faux papiers de la
Gestapo. Mélanie Berger avait pu auparavant expliquer clandestinement depuis
l'hôpital comment elle pouvait être délivrée.


Liens
======

- :ref:`cogitore_2024_05_11`


2024-05-09 Article de France3 régions  **À 102 ans, cette ancienne résistante continue de lutter contre le fascisme**
=========================================================================================================================

- https://france3-regions.francetvinfo.fr/auvergne-rhone-alpes/haute-loire/temoignage-a-102-ans-cette-ancienne-resistante-continue-de-lutter-contre-le-fascisme-2967140.html

8 mai 1945 : capitulation allemande, fin de la Seconde guerre mondiale.
Une période dont se souviennent tous les survivants d'aujourd'hui, ceux qui
peuvent encore témoigner, comme Mélanie Volle.
**Elle lutte depuis son adolescence contre toute forme de dictature et pour la
liberté**.
D'abord dans son pays, l'Autriche puis en France où elle continue
de témoigner pour que le fascisme ne revienne pas au pouvoir.

Mélanie Volle est libre et debout, entourée de ses proches, portée par leur
amour. Sa vie de résistante a commencé en 1934, dans une Autriche en guerre
civile, plongée dans la misère. "On luttait contre le nazisme. On ne comprenait
pas pourquoi un peuple était supérieur aux autres. On défendait la liberté et
la démocratie mais ce que l'on voyait de l'Allemagne ne ressemblait pas à ça",
se remémore cette femme âgée de 102 ans aujourd'hui.


Sauvée d'un convoi de déportation
------------------------------------

À 16 ans, Mélanie Volle s'échappe d'un convoi de déportation. Réfugiée
en France, l'adolescente poursuit sa résistance avant d'être emprisonnée à
Marseille. **"On collait des petites affiches sur le fascisme, sur Hitler... S'il
y avait un contrôle, on faisait semblant de nous embrasser. C'est pour ça
qu'on partait toujours garçon et fille ensemble"**, continue-t-elle, depuis son
appartement à Saint-Étienne

Passeuse de mémoire auprès des jeunes
-------------------------------------------

Après guerre, c'est avec Lucien Volle, résistant combattant du Groupe Lafayette en
Haute-Loire, l'amour de sa vie, qu'elle continue la lutte auprès des jeunes. Le
couple parcourt inlassablement les établissements scolaires pour transmettre
son témoignage.

.. figure:: images/portrait_jeune.webp


::

    Pour moi, il est nécessaire d'expliquer, tant que je peux le faire, qu'Hitler
    est arrivé légalement au pouvoir et qu'à partir du moment où il a été
    là, cela a été une dictature. Alors, je dis "Faites attention à ça".

    Mélanie Volle a reçu depuis de nombreuses décorations, dont la Légion
    d'honneur en 2013.
    Elle a aussi été choisie par le département de la Loire et la mairie de
    Saint-Étienne pour porter la flamme, le 22 juin 2024, avant les
    Jeux olympiques et paralympiques de Paris.
    Symbole de son courage et de son dévouement tout au long de sa vie pour
    l'amitié, la paix et la liberté.

Mais dans un contexte de montée des extrêmes en Europe, rien n'est acquis selon
elle.

::

    "Je ressens actuellement la même chose que j'ai sentie lorsque j'avais
    quinze ans. Est-ce que l'on aura la guerre ou non ?
    Maintenant, on n’est pas loin de la troisième guerre mondiale."

Mélanie Volle croit toujours en l'amour, cette arme indispensable face au
fascisme.

::

    "Il faut que les jeunes puissent trouver l'amour. Pas la haine."
