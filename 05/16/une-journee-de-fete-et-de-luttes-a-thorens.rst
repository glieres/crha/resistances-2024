.. index::
   ! Une journée de fête et de luttes à Thorens

.. _librinfo74_2024_05_16:

==========================================================================
2024-05-16 **Une journée de fête et de luttes à Thorens** par librinfo74
==========================================================================

- https://librinfo74.fr/une-journee-de-fete-et-de-luttes-a-thorens/
- :ref:`librinfo74`

Comme chaque année Thorens vit à l’heure des citoyens résistants.

Près de la salle Tom Morel, dès poltron minet, des stands d’associations de
syndicats et de partis de gauche se mettent en place, pour recevoir les 2 500
visiteurs attendus des pays de Savoie mais aussi de toute la France.

Un rendez-vous annuel que beaucoup de militants ne veulent surtout pas rater.

Comme chaque année, ils n’ont pas été déçus.

Dès vendredi soir, la pièce de théâtre « La serviture volontaire », a conquis
de nombreux spectateurs, ébahis par la modernité et la pertinence des thèmes
abordés par la pièce d’Étienne de la Boëtie.

Le débat de samedi matin sur le thème des libertés publiques a réuni Edwy Plenel,
Arié Halimi et Agnès Rousseaux directrice de Politis.

Comme à son habitude, Pleynel a capté la sympathie de son auditoire par sa
capacité oratoire.

Ce que l’on peut retenir de son intervention est de défendre le droit international
instauré par René Cassin en 1948 qui permet d’imposer des règles humanistes
et anti guerre aux différents États malades par le cancer nationaliste.

Pleynel dénonce un "Maccarthysme" à la française qui, à l’état 2014, instaure
le délit d’apologie au terrorisme, qui permet de qualifier les militants des
luttes écologistes et syndicales de "terroristes".

Agnès Rousseaux a brossé un tableau exhaustif peint au vitriol contre la
mainmise des médias dominants, biberonnés par les compte en banques de milliardaires
qui imposent des fausses informations favorables à la montée de l’extrême droite
néo-fasciste.

De nombreux stands, témoins de la vie militante associative.

Comme chaque année, les associations locales étaient bien présentes, avec
l’Afps 74, les amis de la Terre, ATTAC, Amnesty, l’ecrevis, Greenpeace, NoJO,
Arena, Artisans du monde …

L’allée des partis politiques syndicats médias
==================================================

Une allée bien fréquentée pour rencontrer les militants politiques, PCF, NPA,
écologistes, insoumis pour débattre en vase clos sur les enjeux des européennes
du 9 juin.

L’occasion pour chaque parti de justifier ses divisions pour mieux préparer l’unité !

Plus tranquilles et sources de réflexion et de culture avec les différents
médias alternatifs exposés, les stands des médias avec Silence, Politis,
librAdio, médis d’inspiration libertaire de Genève, la librairie Jean-Jacques Rousseau,
collectif Écran total, éditions "La lenteur"

Une foule, des militants, des femmes et des hommes heureux de partager un
instant de fête

Quand on regarde en contre plongée le public assis au repas de ce samedi à Thorens,
parler avec les militants des stands, danser sur les flonflons d’une fanfare
hilare et déjantée, on sait que les souvenirs de cette journée nourriront les
luttes futures pour construire une société fraternelle et solidaire.
