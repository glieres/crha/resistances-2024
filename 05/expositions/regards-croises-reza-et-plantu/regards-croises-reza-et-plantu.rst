.. index::
   pair: Exposition; Regards croises Reza et Plantu

.. _reza_2024_05:

============================================================
**Regards croises Reza et Plantu**
============================================================

- http://www.citoyens-resistants.fr/Documents/Programme2024.pdf
- https://acrobat.adobe.com/id/urn:aaid:sc:EU:7af6c8bf-04d5-4063-b1ae-e740b2cf79ae

.. figure:: images/affiche.webp
