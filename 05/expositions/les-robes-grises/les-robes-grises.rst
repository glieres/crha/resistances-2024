.. index::
   ! Les Robes grises
   ! Jeannette Lherminier
   ! Germaine Tillion

.. _robes_grises:

==============================================================================
**Les Robes grises** avec Jeannette Lherminier et Germaine Tillion à la MJC
==============================================================================

- http://www.citoyens-resistants.fr/Documents/Programme2024.pdf
- http://hg.ac-besancon.fr/wp-content/uploads/sites/63/2016/01/pdf_DP_Expo_Robes_grises.pdf
- https://fr.wikipedia.org/wiki/Mus%C3%A9e_de_la_R%C3%A9sistance_et_de_la_D%C3%A9portation_(Besan%C3%A7on)
- https://fr.wikipedia.org/wiki/Association_nationale_des_anciennes_d%C3%A9port%C3%A9es_et_intern%C3%A9es_de_la_R%C3%A9sistance
- https://fr.wikipedia.org/wiki/Portail:Seconde_Guerre_mondiale


.. figure:: images/affiche.webp


Description
==============

- https://fr.wikipedia.org/wiki/Germaine_Tillion

Dessins et manuscrits de :ref:`Jeannette Lherminier <herminier>` et :ref:`Germaine Tillion <tillion>`


.. _tillion:

Germaine Tillion
===================

.. figure:: images/germaine_tillion.webp

Germaine Tillion, née le 30 mai 1907 à Allègre (Haute-Loire) et morte le
19 avril 2008 à Saint-Mandé (Val-de-Marne), est une résistante et ethnologue
française.

Titulaire de nombreuses décorations pour ses actes héroïques durant la
Seconde Guerre mondiale, elle est en 1999 la deuxième Française à devenir
grand-croix de la Légion d'Honneur après Geneviève de Gaulle-Anthonioz.

Un hommage de la Nation lui a été rendu au Panthéon le 27 mai 2015, où elle
est entrée en même temps que Geneviève de Gaulle-Anthonioz, Jean Zay et
Pierre Brossolette.

Elle crée l'association Germaine Tillion en 20041.


La Résistance (1940-1942)
---------------------------------

Sa seconde mission prend fin en mai 1940 ; elle quitte Kebach le 21 mai et
arrive à Paris le 9 juin, en pleine débâcle de l'armée française.
Ayant quitté Paris avec sa mère, c'est au cours de l'exode qu'elle entend
le discours de Pétain du 17 juin (« il faut cesser le combat »), auquel elle
réagit par un refus immédiat et catégorique.

Peu après l’armistice, elle recherche d'autres personnes ayant le même point
de vue qu'elle et prend contact avec Paul Hauet (1866-1945), colonel en
retraite devenu industriel, anti-pétainiste de longue date ; elle trouve aussi
des sympathies au Musée de l'Homme (Réseau du musée de l'Homme : Yvonne Oddon,
Paul Rivet...)31.

Après le retour à Paris de Boris Vildé (juillet) et d'Anatole Lewitsky (août),
s'établit un réseau autour de Paul Hauet et Boris Vildé, avec pour objectifs
l'assistance aux prisonniers de guerre notamment africains, le renseignement
militaire et la propagande (journal Résistance, publié à partir de décembre 1940).

Germaine Tillion ne participe pas à Résistance, elle est surtout l'adjointe
de Paul Hauet dans le cadre de l'Union nationale des Combattants coloniaux,
qui sert de couverture à leurs activités.

À la fin de l'année 1940, elle donne les papiers de sa famille à une famille
juive qui sera ainsi protégée jusqu'à la fin de la guerre.

Le réseau est démantelé en 1941 : arrestations en janvier-mars de Boris Vildé,
Anatole Lewitsky et d'autres personnes du Musée de l'Homme, puis en juillet
de Paul Hauet et de Charles Dutheil de La Rochère.

Germaine Tillion devient alors responsable de ce qui reste du réseau.

En janvier 1942, le procès contre Vildé, Lewitsky, Oddon, etc. aboutit à
sept exécutions pour espionnage.


Dernières activités de Résistance (juillet 1941-août 1942)
-----------------------------------------------------------------

Amie des Lecompte-Boinet, elle entre en contact avec Combat Zone nord et
par Jacques Legrand, avec un groupe lié à l'Intelligence Service, le réseau Gloria.

Mais celui-ci est **infiltré par un agent de l'Abwehr, Robert Alesch, prêtre,
vicaire de La Varenne-Saint-Hilaire (à Saint-Maur-des-Fossés)**, qui réussit
à livrer de nombreux résistants, dont, le 13 août 1942, Germaine Tillion,
lors d'un rendez-vous à la gare de Lyon.

Jugé en 1949, il sera condamné à mort et exécuté.

Incarcération (août 1942-octobre 1943)
-------------------------------------------

Après un passage rue des Saussaies, elle est incarcérée à la prison de la Santé,
subissant quatre interrogatoires (par l'Abwehr) en août et trois en octobre.

Inculpée pour cinq chefs d'accusation, elle est transférée à Fresnes, où en
janvier 1943, elle apprend l'arrestation de sa mère.

À Fresnes, elle obtient la disposition de sa documentation et poursuit la
rédaction de sa thèse.

La déportation : Le Verfügbar aux Enfers
------------------------------------------

Déportées à Ravensbrück.

Le 21 octobre 1943, intégrée dans la catégorie NN, Germaine Tillion est déportée
sans jugement et emmenée avec 24 autres prisonnières de Fresnes au camp de Ravensbrück,
au nord de Berlin, par train de voyageurs (sans passer par le camp de Compiègne).

Sa mère, résistante comme elle, y est déportée en février 1944 et est gazée en mars 1945.

Placée dans la catégorie des Verfügbar (de l'allemand verfügbar : disponible),
prisonniers non affectés à un Kommando de travail, mais « disponibles »
pour les pires corvées, elle réussit à échapper pendant plusieurs mois à tout
travail pénible, et utilise toutes ses capacités pour comprendre le monde
dans lequel elle se trouve.

En mars 1944, elle fait clandestinement une conférence pour quelques-unes
des déportées françaises.

Elle fait la connaissance de Margarete Buber-Neumann, qui dès cette époque
lui apprend ce qu'est le système des camps de travail forcé soviétique.

Elle y rencontre également Denise Vernay, résistante, qui participera à ses
recherches ultérieures sur le camp.

En octobre 1944, elle écrit, sur un cahier soigneusement caché, une opérette
Le Verfügbar aux Enfers. Germaine Tillion y mêle des textes relatant avec
humour les dures conditions de détention et des airs populaires tirés du
répertoire lyrique ou populaire.
L'opérette sera mise en scène pour la première fois en 2007 au théâtre du
Châtelet, à Paris.

Grâce à une mise au revier (infirmerie-mouroir) et à des complicités,
Germaine Tillion échappe à un transport à destination du camp de Mauthausen,
à une époque où les autorités du camp mènent une politique d'extermination
systématique (création d'une chambre à gaz au début de 1945).

Puis un événement inattendu a lieu : la tentative de Himmler de négocier
son avenir avec les puissances occidentales.

L'évacuation en Suède (avril 1945)
----------------------------------------

Début avril, 300 Françaises sont évacuées par la Croix-Rouge internationale,
mais les détenues Nacht und Nebel sont exclues.
Un peu plus tard cependant, des négociations entre Heinrich Himmler et le
diplomate suédois Folke Bernadotte permettent à un autre groupe de détenues
françaises, dont elle fait partie, d'être évacuées par la Croix-Rouge suédoise ;
le 24 avril, elles sont emmenées en autocar à Padborg au Danemark (encore occupé),
puis en train à Göteborg en Suède où elles sont prises en charge par un
établissement hospitalier.

Elles réussissent à sortir du camp des documents, notamment des photographies
relatives à des expériences médicales menées sur des détenues, le texte de
l'opérette, etc.

Dès le début du séjour à Göteborg, Germaine Tillion lance un travail de
recherche sur le camp de Ravensbrück à travers un questionnaire qu'elle
utilisera ensuite pendant plusieurs années.

Une partie des archives de ces travaux est aujourd'hui disponible dans le
fonds `ADIR <https://fr.wikipedia.org/wiki/Association_nationale_des_anciennes_d%C3%A9port%C3%A9es_et_intern%C3%A9es_de_la_R%C3%A9sistance>`_
de La contemporaine, l'autre (les « fiches blanches ») dans le fonds
Germaine Tillion du `Musée de la Résistance et de la Déportation de Besançon <https://fr.wikipedia.org/wiki/Mus%C3%A9e_de_la_R%C3%A9sistance_et_de_la_D%C3%A9portation_(Besan%C3%A7on)>`_.

.. _herminier:

Jeanne L'Herminier
===================

- https://fr.wikipedia.org/wiki/Jeanne_L%27Herminier

Marie-Altée l'Herminier, dite Jeannette (née à Nouméa, le 15 octobre 1907
et morte le 7 mars 20071 à Vanves), est une résistante et déportée française.


Biographie
----------------

Elle est la sœur du capitaine de corvette Jean l'Herminier commandant le
sous-marin Casabianca qui s'est échappé de Toulon lors du sabordage de la
flotte le 27 novembre 1942.

Elle s'engage dans la Résistance. Le 19 septembre 1943, elle est arrêtée
avec sa belle-mère à Paris par la Gestapo, car les deux femmes cachaient
un aviateur américain et appartenaient aux réseaux Buckmaster.

Elle est déportée en février 1944 à Ravensbrück, camp de femmes installé
au nord de Berlin.
Dans ce camp de concentration, elle commence à croquer les silhouettes de
ses camarades détenues.

Pour échapper à l'horreur des camps, elle embellit ses camarades pour les
montrer telles qu'elles auraient dû être.
Pour dessiner, elle utilise des morceaux de journaux puis des boîtes de
cartouches récupérées dans le Kommando de Holleischen, une fabrique de
munitions où elle travaille.

Elle fit ainsi plus de 150 dessins, réalisés et sauvés grâce à la complicité
souvent dangereuse de ses camarades (notamment Elisabeth Barbier, qui sortira
du camp la plupart des dessins de Ravensbrück).

Après avoir récupéré la grande majorité de ces dessins clandestins,
elle les confie en 1987 au Musée de la Résistance et de la Déportation de
Besançon et au Musée de l'Ordre de la Libération, à Paris.

