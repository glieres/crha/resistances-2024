.. index::
   pair: Gauche; Face à l’extrême droite : refaire la gauche et son imaginaire
   ! Face à l’extrême droite : refaire la gauche et son imaginaire
   ! Philippe Corcuff
   ! Philippe Marlière

.. _gauche:

====================================================================
**Face à l’extrême droite : refaire la gauche et son imaginaire**
====================================================================


2024-04-29  **Face à l’extrême droite : refaire la gauche et son imaginaire, radicalement** par Philippe Corcuff
====================================================================================================================

- :ref:`raar_2024:corcuff_2024_04_29`


2024-04 Le livre "Les tontons flingueurs de la gauche, Lettres ouvertes à Hollande, Macron, Mélenchon, Roussel, Ruffin, Onfray" de Philippe Corcuff et Philippe Marlière
===========================================================================================================================================================================

- :ref:`antisem:tontons_flingueurs_2024`


.. figure:: images/couverture.webp
   :width: 400


L’extrême droite progresse à grandes enjambées. À gauche, peu de gens mesurent
la gravité de la situation.

Ce petit livre se présente comme un cri d’alarme, inspiré par le souffle encore
chaud de Rosa Luxemburg et de Pierre Mendès France ; une révolutionnaire éprise
de pluralisme et un réformateur soucieux d’éthique.

La gauche a trop souffert des hommes providentiels qui prétendent lui offrir
un nouvel avenir mais dont l’action, finalement, l’affaiblit.

Dans cet essai incisif, deux intellectuels engagés adressent une lettre ouverte
à six personnalités politiques issues de leur propre famille :

- un ancien président qui a perdu la gauche,
- un président marchepied vers l’extrême droite,
- un insoumis autoritaire,
- un communiste sécuritaire,
- un rebelle nationaliste
- et un libertaire ultraconservateur.

Ils ont tous contribué, à des degrés divers, au présent naufrage.

Reste à réinventer une gauche d’émancipation, qui fera le deuil du leader
charismatique mais qui aura besoin d’incarnations, plurielles et polyphoniques.


2023-12-10 leftrenewal **Pour une gauche démocratique et internationaliste, Contribution au renouveau et à la transformation de la gauche**
=================================================================================================================================================

- https://leftrenewal.net/
- https://leftrenewal.net/french-version/
- :ref:`raar_2023:leftrenewal_fr_2023_12_10`


📢 Réunion publique **Qui a peur de la lutte contre l'antisémitisme ?** pour une gauche réellement antiraciste 🗓️ le jeudi 23 mai 2024 à 19h
==================================================================================================================================================

- :ref:`raar_2024:raar_2024_05_10`

Face à l’augmentation massive des actes antisémites partout dans le monde
depuis le 7 octobre 2023, et alors que les crimes de guerre de l’armée israélienne
se multiplient, menaçant l’existence des Palestinien·ne·s de Gaza, nos
collectifs Golem, JJR et RAAR **souhaitent engager un débat dans et avec
l’ensemble du camp progressiste**.

**Nous avons besoin de parler et d’échanger afin d’échapper aux pièges qui
empoisonnent les débats actuels**.

Dans nos espaces politiques, **nous constatons une indifférence toujours plus
grande face à l’explosion des actes anti-juifs**, comme si l’appropriation
malhonnête de la lutte contre l’antisémitisme par les forces réactionnaires
disqualifiait cette question pourtant bien réelle.

Nous sommes aussi inquiet·e·s **de la stigmatisation systématique de groupes
juifs exprimant une condamnation des crimes du Hamas**, qui se voient dénigrés
comme "sionistes", terme proclamé comme une injure, voire comme "fascistes".

A cela s’ajoute **une montée généralisée** du :term:`antisem:complotisme` et du :term:`antisem:confusionnisme`,
qui ne restent malheureusement pas cantonnés à la fachosphère et **déboussolent
aussi le camp progressiste**.

**Concernant la droite et l’extrême-droite, nous condamnons la manière dont
elles utilisent la lutte contre l’antisémitisme à des fins islamophobes**
ou pour attaquer les mouvements progressistes.

**Tout cela a été rendu possible en raison de l’abandon par une partie de la
gauche de la lutte contre l'antisémitisme**.

Cette situation nécessite des prises de positions claires **dans lesquelles on
n’oublie rien et ne sacrifie rien de nos exigences progressistes**.

Il s’agit **d’une véritable "ligne de crête"** :

- ne rien lâcher, **ni sur la lutte contre l'antisémitisme, ni sur le combat
  contre tous les racismes** ;
- **lutter contre la tendance à nier les exactions du 7 octobre 2023, d’un côté,
  manifester l’horreur que nous inspire ce que le peuple palestinien endure
  en ce moment et depuis trop longtemps, de l’autre** ;
- **garder intacte la mémoire des victimes du Hamas d’un côté, dénoncer la
  banalisation du carnage quotidien que subissent les Palestinien·ne·s,
  de l’autre** ;
- **réclamer la libération des otages, d’un côté, réclamer un cessez-le feu
  immédiat et total à Gaza, de l’autre**.

Nous appelons tou·te·s les camarades, ami·e·s et organisations progressistes à
venir discuter de ces questions avec nous :ref:`à la Bourse du Travail (Salle Hénaff) <bourse_travail_2024_05_23>`,
le 23 mai 2024, à 19h.

**Venez en débattre avec nous !**

**Rejoignez-nous sur cette ligne de crête !**

**Ne laissons pas le terrain de la lutte contre l'antisémitisme à la droite et l'extrême-droite !**

Venez nombreux·ses !

**Inscription** à l’adresse suivante: lagauchecontrelantisemitisme@gmail.com



