.. index::
   pair: Yannis Youlountas ; Nous n'avons pas peur des ruines de Yannis Youlountas cinéma Le Parnal à 13h30
   ! Nous n'avons pas peur des ruines** de Yannis Youlountas cinéma Le Parnal à 13h30

.. _youlountas_2024_05_11:

==========================================================================================================
2024-05-11  🎥 **Nous n'avons pas peur des ruines** de Yannis Youlountas cinéma Le Parnal à **13h30**
==========================================================================================================

- http://www.citoyens-resistants.fr/Documents/Programme2024.pdf
- http://vimeo.com/914528319#embed

Militant en lien : **Giorgos Kalaitzidis** militant attaqué par l'Etat grec
==============================================================================

- :ref:`giorgos_kalaitzidis`
