.. index::
   ! Gaza : sélection de courts métrages

.. _alasttal_2024_05_11:

===========================================================================================================
2024-05-11  🎥 **Gaza : sélection de courts métrages** de Iyad Alasttal au cinéma Le Parnal à **20h30**
===========================================================================================================

- http://www.citoyens-resistants.fr/Documents/Programme2024.pdf
- http://www.youtube.com/watch?v=HGfvb7_smT4


.. figure:: images/affiche.webp


Iyad Alasttal  🎥
======================

- :ref:`iyad_allastal`

Journaliste et réalisateur palestinien, :ref:`Iyad Alasttal <iyad_allastal>` est né lors de la
première Intifada, à Khan Younès, dans le sud de la bande de Gaza.

En 2013, à l’issue d’une formation d’audiovisuel à l’université de Corte en Corse,
il rentre chez lui et produit plusieurs documentaires en français, plusieurs
fois primés.

Pour cette soirée, une sélection de courts métrages est présentée, en sa présence.

:ref:`Iyad témoignera le dimanche sur le plateau <iyad_allastal>`.
