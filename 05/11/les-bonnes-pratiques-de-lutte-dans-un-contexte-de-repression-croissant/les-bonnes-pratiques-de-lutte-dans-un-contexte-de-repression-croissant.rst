.. index::
   ! Atelier participatif : Les bonnes pratiques de lutte dans un contexte de répression croissant [10h/12h, 14h/16h]

.. _xr_2024_05_11:

===============================================================================================================================================================
2024-05-11 Atelier participatif **Les bonnes pratiques de lutte dans un contexte de répression croissant** avec Extinction Rebellion **[10h/12h, 14h/16h]**
===============================================================================================================================================================

- https://www.citoyens-resistants.fr/Documents/XR.pdf
- https://extinctionrebellion.fr/branches/annecy/

Présentation
=================

Atelier participatif organisé par l’association **Extinction Rébellion (XR)**


Il s’agit d’apprendre à savoir se protéger et à connaître les bonnes pratiques
pour pouvoir continuer à lutter et manifester sereinement dans un contexte de
répression croissante.

- 2 ateliers (15 personnes maximum par session) : 10-12 h et 14-16 h
- Lieu : Espace Forestier (face à la librairie)

.. warning:: ATTENTION : pas d’inscription, se présenter sur place un peu
   avant 14 h ou 16 h, selon la session.

Animation
===========

Les ateliers sont animés par des membres de l’association XR d’Annecy.
