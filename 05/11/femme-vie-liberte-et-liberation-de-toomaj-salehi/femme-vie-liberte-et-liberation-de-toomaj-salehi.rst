.. index::
   pair:  Iran Solidarité ; Le mouvement Femme Vie Liberté en Iran à 14h
   pair: Soutien; Femme Vie Liberté
   pair: Soutien; Femme Vie Liberté
   ! Iran Solidarité
   ! Zoya Daneshrad
   ! Soutien au peuple Iranien
   ! Iran
   ! MasahAmini

.. _iran_solidarite_2024_05_11:

========================================================================================================================================================================
2024-05-11 |JinaAmini| **Le mouvement Femme Vie Liberté en Iran et libération de Toomaj Salehi** avec Zoya d'Iran Solidarités salle Tom Morel  **à 16h30** ♀️ ✊🏼 📣
========================================================================================================================================================================

- :ref:`liens_iran:linkertree_iranluttes`
- :ref:`iran_2024:cgt_2024_05_07`
- :ref:`crha_2023:femme_vie_liberte_2023_05_20`
- https://librinfo74.fr/la-resistance-du-peuple-iranien-au-coeur-du-rassemblement-des-glieres/

#CRHA  #résistance #Glieres

🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی (Persan) #ZanZendegiÂzâdi 🇮🇷
🇫🇷 Femme, Vie, Liberté #FemmeVieLiberte (Français, 🇫🇷)
🇮🇹 Donna, Vita, Libertà (Italien, 🇮🇹 )
🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )

#NonALaPeineDeMort #NoDeathPenalty
#FreeToomaj #FreeToomajSalehi #PesareAzadi |toomaj|
#FreeIranOstages #FreeCecileKohler #FreeJacquesParis #FreeLouisArnaud
#FreeThemAll
#ToomajSalehi #FazelBahramian #MamostaMohammadKhazrnejad, #ManouchehrMehmanNavaz
#MehranBahramian #MojahedKourkour #RezaRasaei
#FreeToomajSalehi  #FreeFazelBahramian #FreeMamostaMohammadKhazrnejad,
#FreeManouchehrMehmanNavaz #FreeMehranBahramian #FreeMojahedKourkour #FreeRezaRasaei


L'intervenante : Zoya Daneshrad d'Iran Solidarités
==========================================================


- Zoya Daneshrad :ref:`d'Iran Solidarité <liens_iran:linkertree_iranluttes>` |iran_solidarite|


Les vidéos
===============

.. youtube:: wP29qimIdj8


.. _zoya_baraye_2024_05_11:

La chanson Barâyé (qu'on peut traduire par "Pour" en français) de Shervin Hajipour interprétée par Zoya
--------------------------------------------------------------------------------------------------------------

- :ref:`iran_luttes:baraye`

.. youtube:: VNr36XaGw3I

Les paroles de la chanson Barâyé de Shervin Hajipour interprétée par Zoya
---------------------------------------------------------------------------

.. youtube:: kL4qQDn5UcQ


Texte
=======

Il y a un an et demi Mahsa Jina Amini a été tuee par la République islamique
d'Iran pour un voile mal ajusté.

Il s'en est suivi le mouvement "Femme Vie Liberté" qui a fait trembler le monde
entier pour protester contre le foulard obligatoire et pas contre le foulard,
et pour contester tout le système patriarcal.

La révolte des femmes, soutenue par les hommes et toutes les couches de la
société, s'est répandue partout en Iran .
Malgre plus de 500 morts et 20 000 arrestations, les tortures et les viols
dans les prisons, ce mouvement continue son chemin.

Même si après des exécutions, le mouvement a perdu de vitesse, il n'est pas
mort. Il est entré dans la phase de résistance. Les femmes continuent leur
combat contre l'apartheid sexiste de ce régime en refusant de porter le foulard
obligatoire au risque de leur vie de prison, de torture , de viol, de confiscation
de leur voiture, des amendes etc .

Tandis qu'avec la loi de ce gouvernement, on peut marier des fillettes de 12
ans avec des hommes de 50, l’adultère est passible de la peine de mort.
Les religieux ont institutionnalisé la prostitution avec mariage provisoire de
quelques minutes à 99 ans.

Depuis le mouvement femme vie liberte , rien n'est revenu à la normale.
La société est en bouillonnement.

Il n'y a pas un seul jour sans grève, manifestations et contestation de tout genre.
La situation économique est désastreuse avec 60% de la population sous le seuil
de pauvreté. Les ouvriers restent parfois sans salaires pendant 6 à 9 mois.
Le salaire minimum suffit à peine aux frais du loyer tandis,que les membres de
ce gouvernement ne font que piller et voler les ressources du pays cumulant des
milliards,dans les pays,occidentaux.

La répression est sans précédent actuellement. Et le régime attaqué les femmes
avec la sauvagerie et augmente les pendaisons .  En 2023 il y a eu 834 pendaisons,
l'Iran etant le deuxième pays ayant des nombres les plus élèves dex exections

Ce regime a étendu son emprise délétère sur l'Europe en soutenant la Russie et
son invasion et en finançant des groupes terroristes et fanatiques de la région.
Nourri par l'argent de drogue entre autre, il fait régner la terreur en executant
les petits trafiquants sur les grues dans les places publiques ainsi que les
manifestants et l'opposition.


Mais la communauté internationale continue a soutenir le gouvernement islamique
d'iran. l'ONU choisit honteusement Téhéran pour présider la conférence
internationale sur le désarmement nucléaire et lui accorde la présidence du forum
social sur l'utilisation des multimédias pour les droits de l'homme permettant
au gouvernement islamique d'Iran deprendre des ailes pour renforcer la pression
et augmenter considérablement les exécutions et les condamnations à mort la
dernière en date celle de Toomaj Salehi le rappeur militant bien connu en Iran.


A l'heure actuelle, au moins sept personnes sont sous le coup d’une
condamnationvà mort pour avoir pris part aux manifestations nationales "Femme
Vie Liberté»

Les prisons sont remplis d'intellectuels , des cinéastes et d'artistes sous torture

4 otages français sont en prison en Iran pour servir d'appât pour le gouvernement islamique.

Nous demandons à la communauté internationale

- d'arrêter de flirter avec ce régime assassin
- de soutenir le peuple et les femmes iraniens dans leur combat,
- d'empecher les arrestation et ces exécutions macabres,
- de demander l'annulation de la condamnation à mort de Toomaj et tous les autres
  condamnes et leur libération.

au nom de Femme Vie Liberté

Iran Solidarités


.. figure:: images/20240511_084754_800_zoya.webp
.. figure:: images/20240511_091532_800_mise_en_place.webp
.. figure:: images/20240511_091536_800_mise_en_place_2.webp
.. figure:: images/20240511_091550_800_mise_en_place_2.webp
.. figure:: images/20240511_093105_800_panneaux.webp
.. figure:: images/20240511_120805_800_panneaux_femme_vie.webp
.. figure:: images/20240511_125905_800_panneau_toomaj.webp
.. figure:: images/20240511_130308_800_zoya_ecrit.webp
.. figure:: images/20240511_130320_800_non_non_non.webp
.. figure:: images/20240511_130458_800_zoya_discussions.webp
.. figure:: images/20240511_130554_800_accroche.webp
.. figure:: images/20240511_130826_800_zoya_table.webp
.. figure:: images/20240511_130842_800_mahsa.webp
.. figure:: images/20240511_131547_800_panneau_jina.webp
.. figure:: images/20240511_134529_800_refaire_noeud_coulant.webp
.. figure:: images/20240511_140714_800_zoya_table.webp
.. figure:: images/20240511_140720_800_lise_leider.webp
.. figure:: images/20240511_141122_800_zoya_amnesty.webp
.. figure:: images/20240511_143804_800_vue_de_loin.webp
.. figure:: images/20240511_143814_800_vue_laterale.webp
.. figure:: images/20240511_143818_800_vue_panneaux.webp
.. figure:: images/20240511_153303_800_fumex.webp
.. figure:: images/20240511_153407_800_fumex.webp
.. figure:: images/20240511_162909_800_avant_discours.webp
.. figure:: images/20240511_162929_800_avant_discours.webp
.. figure:: images/20240511_162941_800_avant_discours.webp
.. figure:: images/20240511_162950_800_avant_discours_perret.webp
.. figure:: images/20240511_163800_800_avant_discours.webp
.. figure:: images/20240511_171426_800_panneaux.webp
.. figure:: images/20240511_182726_800_devant_panneaux_foule.webp
.. figure:: images/20240511_182731_800_devant_panneaux_foule.webp
.. figure:: images/20240511_182828_800_paysage_et_panneaux.webp
.. figure:: images/20240511_195310_800_entree_de_loin.webp
.. figure:: images/20240511_195337_800_entree_panneaux.webp
.. figure:: images/20240511_195350_800_entree_zoya.webp
.. figure:: images/20240511_195357_800_entree_zoya.webp
.. figure:: images/20240511_195446_800_zoya_table.webp
.. figure:: images/20240511_195924_800_otages_francais.webp
.. figure:: images/20240511_200007_800_pendaisons_toomaj.webp


Liens
========

Les condamnés à mort en Iran 📣
----------------------------------

#ToomajSalehi #FazelBahramian #MamostaMohammadKhazrnejad, #ManouchehrMehmanNavaz #MehranBahramian #MojahedKourkour #RezaRasaei
#FreeToomajSalehi  #FreeFazelBahramian #FreeMamostaMohammadKhazrnejad, #FreeManouchehrMehmanNavaz #FreeMehranBahramian #FreeMojahedKourkour #FreeRezaRasaei


- :ref:`iran_luttes:toomaj` |toomaj|
- Fazel Bahramian,فاضل بهرامیان
- Mamosta Mohammad Khazrnejad, ماموستا محمد خزرنژاد
- Manouchehr Mehman Navaz, منوچهر مهمان نواز
- Mehran Bahramian مهران بهرامیان
- Mojahed (Abbas) Kourkour, مجاهد کورکور
- Reza (Gholamreza) Rasaei


Les 4 premiers condamnés à mort de la révolution MahsaJinaAmini
----------------------------------------------------------------------

.. figure:: images/les_4_premiers_condamnes.png

:download:`docs/les_4_premiers_condamnes.odt`
:download:`docs/les_4_premiers_condamnes.pdf`

.. _otages_francais:

Les 4 otages français 📣
----------------------------

#CRHA #Glieres #FreeCecileKohler #FreeJacquesParis #FreeLouisArnaud 
@FreeCecile_  @freelouisarnaud

- :ref:`iran_luttes:otages_francais`

- :ref:`iran_luttes:cecile_kohler` |CecileKohler|
- :ref:`iran_luttes:jacques_paris` |JacquesParis|
- :ref:`iran_luttes:louis_arnaud` |LouisArnaud|
- :ref:`iran_luttes:x` 

.. figure:: images/les_otages.png

:download:`docs/les_4_otages_francais.odt`
:download:`docs/les_4_otages_francais.pdf`


.. figure:: images/4_otages_francais.webp

**Samedi 20 mai 2023 toute la journéee à la MJC Exposition "Femme, Vie, Liberté" en soutien au peuple iranien** #FemmeVieLiberté #MasahAmini
------------------------------------------------------------------------------------------------------------------------------------------------------------

- :ref:`crha_2023:femme_vie_liberte_2023_05_20`


Dernières nouvelles
======================

#FreeMohammadRasoulof

Mohammad Rasoulof, 52 ans, avait été arrêté en juillet 2022 pour avoir soutenu
des manifestations déclenchées après l’effondrement d’un immeuble ayant fait
plus de 40 morts en mai dans le sud-ouest de l’Iran.

Après ce drame, un groupe de cinéastes iraniens mené par Mohammad Rasoulof
avait publié une lettre ouverte appelant les forces de sécurité « à déposer les armes »
face à l’indignation nationale contre « la corruption » et « l’incompétence »
des responsables.

***8 ans de prison** dont 5 ferme

La république islamique remplit les prisons iraniennes d’étudiants, d artistes,
d’avocats, de chanteurs, d’intellectuels, d’activistes, d’écologistes, de sportifs …
et les plus grands voyous et assassins gouvernent le pays.

#mohammadrasoulof #festivaldecannes

- https://lejournal.info/article/chowra-makaremi-le-regime-iranien-a-perdu-le-soutien-du-peuple/


Sur les réseaux sociaux
=============================

- https://x.com/ZerrinBATARAY/status/1789755342714200472

.. figure:: images/toomaj_salehi.png
