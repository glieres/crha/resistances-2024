.. index::
   ! Pour une sécurité sociale alimentaire au chapiteau de 17h à 19h avec Alice Desbiolles, Benjamin Sèze, Anne Souyris et Benjamin Joyeux


.. _sec_so_2024_05_11:

===========================================================================================================================================================
2024-05-11 **Pour une sécurité sociale alimentaire** au chapiteau avec Alice Desbiolles, Benjamin Sèze, Anne Souyris et Benjamin Joyeux **de 17h à 19h**
===========================================================================================================================================================


- http://www.citoyens-resistants.fr/Documents/Programme2024.pdf
- http://citoyens-resistants.fr/Pourunesecuritesocialealimentaire.pdf

Présentation
================

La pandémie du Covid a démontré jusqu’à l’absurde à quel point notre système
public de santé était un bien commun précieux et comment celui-ci avait été
systématiquement démantelé par des années de politiques néolibérales
d’économie budgétaire au détriment des patient.e.s.

Malheureusement, quatre ans plus tard, des lits d’hôpitaux continuent de fermer
et les inégalités ne cessent de se creuser en termes d’accès à la santé.

Il est urgent de tout changer et d’innover, afin de garantir à tout.es le
droit de vivre en bonne santé partout sur le territoire.

Pour cela, il existe une solution révolutionnaire au meilleur sens du terme,
dans la droite ligne de l’héritage du Conseil national de la Résistance
et d’Ambroise Croizat : la sécurité sociale alimentaire !

Ou comment réparer la santé par l’assiette, en permettant à toutes et tous
d’accéder à une alimentation de qualité

Les intervenant·e·s
=======================

- Alice Desbiolles, médecin épidémiologiste spécialisée en santé publique.
- Benjamin Sèze, journaliste spécialiste des questions sociales au Secours Catholique.
- Anne Souyris, sénatrice écologiste et conseillère de Paris,
  spécialiste des questions de santé.

Animée par Benjamin Joyeux
CRHA et commission santé du Conseil Régional AuRA

