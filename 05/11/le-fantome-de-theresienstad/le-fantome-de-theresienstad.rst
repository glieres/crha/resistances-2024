.. index::
   ! Le fantôme de Theresienstadt

.. _cogitore_2024_05_11:

==========================================================================================================
2024-05-11 🎥 **Le fantôme de Theresienstadt** de Baptiste Cogitore au cinéma Le Parnal **à 16h**
==========================================================================================================


- http://www.citoyens-resistants.fr/Documents/Programme2024.pdf

.. figure:: images/affiche.webp

Le fantôme de Theresienstadt
===============================

En 1942, Hanuš Hachenburg est déporté dans le ghetto juif de Theresienstadt.

Là-bas, il participe à l’une des aventures collectives les plus étonnantes
de l’histoire des camps nazis : la création du magazine clandestin Vedem.

Dans une chambrée de garcons transformée en république imaginaire autogérée,
Hanuš écrit des poemes d'une incroyable maturité pour un enfant de treize ans.

D'abord laissé à la marge, il devient vite un contributeur essentiel du
journal.(...)

Le Fantome de Theresienstadt raconte l’histoire de ce poete disparu et de son
œuvre géniale


Liens
========

- :ref:`melanie_beger_volle`
