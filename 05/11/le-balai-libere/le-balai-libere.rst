.. index::
   pair: Film ; Le balai libéré de Coline Grando - 2023 - 1h28 au cinéma Le Parnal à 17h30

.. _balai_libere_2024_05_11:

==========================================================================================================
2024-05-11  🎥 **Le balai libéré** de Coline Grando - 2023 - 1h28 au cinéma Le Parnal **à 17h30**
==========================================================================================================

- http://www.citoyens-resistants.fr/Documents/Programme2024.pdf
- http://www.youtube.com/watch?v=FHEIyTv4SXI

.. figure:: images/affiche.webp


Description
============

de Coline Grando - 2023 - 1h28

Dans les années 70, les femmes de ménage de l’université catholique de Louvain
mettent leur patron à la porte et créent leur coopérative de nettoyage, **Le Balai libéré**.

50 ans plus tard, le personnel de nettoyage de l’UCLouvain rencontre les
travailleuses d’hier : **travailler sans patron, est-ce encore une option ?**
