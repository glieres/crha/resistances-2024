.. index::
   ! Conférence "Etat républicain, conseils ouvriers et municipalisme libertaire, quelles perspectives ?" à la MJC de 17 à 19h

.. _etat_2024_05_07:

=============================================================================================================================================
2024-05-11 **Conférence "Etat républicain, conseils ouvriers et municipalisme libertaire, quelles perspectives ?** à la MJC **de 17 à 19h**
=============================================================================================================================================


- http://www.citoyens-resistants.fr/Documents/Programme2024.pdf
- http://citoyens-resistants.fr/Documents/QuelEtat.pdf

Présentation
=================

« L’État dans tous ses états », c’est ainsi que nous aurions pu intituler cette
conférence.

Qui sera donc centrée sur la question de l'État (qu'est ce que c'est) et des
possibilités d'organisation d'une société en rupture (ou qui veut l'être) avec le
mode de production capitaliste.

Ce que nous avons résumé en "État républicain, conseils ouvriers et municipalisme
libertaire, quelles perspectives ?".

Seront abordées les questions

- de l’immanence de l’État, c’est à dire le fait que toute société humaine a
  besoin d’un État et en produit,
- ou de sa relativité, c’est-à-dire que l’existence de l’État est profondément
  liée aux intérêts d’un groupe ou d’une classe sociale, qu’il doit donc dépérir
  ou être supprimé radicalement,
- ou du rapport de l’État actuel avec ses fondamentaux notamment les droits
  de l’homme.

Ainsi que:

- comment prendre à bras le corps la vieille Utopie,
- comment remplacer le gouvernement des hommes par l’administration des
  choses.


Les intervenant·e·s
=======================

- Marie DUCRUET, militante de Lutte Ouvrière.
- Daniel IBANEZ, co-fondateur et co-organisateur de la Rencontre annuelle
  des lanceurs d’alertes.
  Très investi notamment dans la lutte contre le Lyon Turin
- Yannis YOULOUNTAS, philosophe libertaire et réalisateur franco-grec

Animateur
-----------

- Bernard CLERC, CRHA


Et échanger sur les conséquences dans notre engagement au quotidien.

Liens concernant la gauche, l'extrême droite et l'imaginaire
================================================================

Face à l’extrême droite : refaire la gauche et son imaginaire, radicalement par Philippe Corcuff
--------------------------------------------------------------------------------------------------

- :ref:`gauche`
