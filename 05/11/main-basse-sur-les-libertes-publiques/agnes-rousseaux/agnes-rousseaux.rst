.. index::
   pair: Agnès Rousseaux ; Main basse sur les libertés publiques (avec Arié Alimi Edwy Plenel et Agnès Rousseaux) **de 10h à 12h**
   pair: Media ; Main basse sur les libertés publiques ; Agnès Rousseaux) **de 10h à 12h**


.. _rousseaux_2024_05_11:

======================================================================================================================================
2024-05-11 **Main basse sur les libertés publiques** salle Tom Morel  discours d'Agnès Rousseaux sur les media **de 10h à 12h**
======================================================================================================================================

- https://www.swisstransfer.com/d/b48458b2-e7c6-4aad-b935-bf299678a0a5
- https://twitter.com/AgnesRousseaux

Directrice de l'hebdo @Politis_fr
. Ex-coordinatrice de Basta! (http://basta.media).
Co-autrice du Livre noir des Banques (LLL) et Au péril de l'humain (Seuil).


Réseaux sociaux
====================

- https://x.com/AgnesRousseaux/status/1789237964402315317
