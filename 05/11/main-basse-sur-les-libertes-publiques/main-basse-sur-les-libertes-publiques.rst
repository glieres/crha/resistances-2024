.. index::
   ! Main basse sur les libertés publiques (avec Arié Alimi Edwy Plenel et agnès Rousseaux) **de 10h à 12h**
   ! Arié Alimi
   ! Edwy Plenel
   ! Agnès Rousseaux
   ! Imaginaire

.. _alimi_2024_05_11:

======================================================================================================================================
2024-05-11 **Main basse sur les libertés publiques** salle Tom Morel avec Arié Alimi Edwy Plenel et Agnès Rousseaux **de 10h à 12h**
======================================================================================================================================

- http://citoyens-resistants.fr/Documents/LibertesPubliques.pdf
- http://citoyens-resistants.fr/Documents/Conference_2024.pdf
- http://www.citoyens-resistants.fr/Documents/Programme2024.pdf


#résistance #conférence #violencepoliciere #media #CRHA #ZAD


Présentation
==================

**Nos Libertés Publiques sont attaquées depuis des décennies.
Mais ce mouvement prend actuellement une ampleur sans précédent**.

Dérives policières, accroissement de la surveillance de masse, menaces,
emprisonnement et blessures envers les opposant.e.s qui osent manifester,
remise en cause du droit de grève et du droit syndical… la liste est longue.

L’État s’arroge illégalement le monopole de la violence "légitime".

Or, **contrairement à ce qu’il fait actuellement, l’État ne peut pas s’abstraire
de ses propres lois**.

**Parallèlement, la liberté de l’information est réduite comme peau de chagrin
par la main-mise de la Finance, quand ce n’est pas de l’extrême droite sur
les organes d’information** : Actuellement, 90 % des quotidiens nationaux publiés
chaque jour en France sont entre les mains de Bolloré et consorts.

**La moitié de l’audience à la radio et à la télévision provient également de médias
qui leur appartiennent**.

Les risques de conflits d’intérêt et d’ingérence éditoriale sont très importants
avec ces entrepreneurs enrichis dans l’armement, la téléphonie ou le luxe.

Cette concentration malsaine nourrit à juste titre la défiance des citoyens.

**Or ce droit à une information pluraliste est la base de la démocratie, et
c’est à juste titre que le CNR avait inclus ce point dans son programme "Les Jours Heureux"**.


Intervenant·e·s
================

- :ref:`Arié Alimi <antisem:arie_alimi>`, avocat pénaliste, membre du bureau de la Ligue des Droits de l'Homme.
- Edwy Plenel, journaliste, ex-directeur du Monde, créateur et ex-directeur de Médiapart.
- Agnès Rousseaux, directrice de Politis, ex-coordonnatrice de Basta.


Animée par Catherine W. Selosse, CRHA


Discours
==============

.. toctree::
   :maxdepth: 3

   arie-alimi/arie-alimi
   agnes-rousseaux/agnes-rousseaux

Militant en lien : Genevève Legay **manifestante blessée par une charge de police**
=====================================================================================

- :ref:`genevieve_legay`


Sur les réseaux sociaux
==========================

- https://urlr.me/dzwsx (facebook CRHA)
- https://x.com/CRHA_glieres/status/1789878824391389337
- https://x.com/edwyplenel/status/1789770875023102248


Film en lien : Le Repli **analyse la montée des discours racistes en France depuis le début des années 80 et la restriction des libertés depuis 2015**.
=======================================================================================================================================================================

- :ref:`repli_2024_05_11`


Liens concernant la gauche, l'extrême droite et l'imaginaire
================================================================

Face à l’extrême droite : refaire la gauche et son imaginaire, radicalement par Philippe Corcuff
--------------------------------------------------------------------------------------------------

- :ref:`gauche`
