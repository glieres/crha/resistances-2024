.. index::
   pair: Arié Alimi ; Main basse sur les libertés publiques (avec Arié Alimi Edwy Plenel et Agnès Rousseaux) **de 10h à 12h**
   pair: Violences policières ; Main basse sur les libertés publiques ; Arié Alimi **de 10h à 12h**


.. _alimi_2024_05_11_discours_1:

===============================================================================================================================================================================================================
2024-05-11 **Main basse sur les libertés publiques** salle Tom Morel  discours d'Arié Alimi sur l'affaire Geneviève Legay et la manifesation du 12 novembre 2023 contre l'antisémitisme **de 10h à 12h**
===============================================================================================================================================================================================================

- :ref:`antisem:arie_alimi`
- https://twitter.com/AA_Avocats
- https://fr.wikipedia.org/wiki/Ari%C3%A9_Alimi
- https://fr.wikipedia.org/wiki/Affaire_Genevi%C3%A8ve_Legay

Discours
============

- https://fb.watch/r_XclKJwFW/ (La conférence Main basse sur les libertés publiques)
- https://www.swisstransfer.com/d/538c4115-38c1-4a65-b50b-f23349d6908f


Réponses
==========

.. youtube:: bY3LOK2eMVU

Thèmes et personnes abordés:

- Geneviève Legay
- manifestation contre l'islamophobie le 10 novembre 2019
- manifestation contre l'antisémitisme le 12 novembre 2023

Texte (à retravailler)
==========================

- https://youtu.be/bY3LOK2eMVU?t=9

... mais d'abord le droit et je voudrais rendre hommage à une personne qui
est dans cette salle aujourd'hui parce que on a ; je l'ai découvert parce que
j'ai découvert les forces de gauche un peu sur le tard mais j'ai découvert
que le droit n'était pas forcément quelque chose de très habituel dans les
matrices idéologiques des gauches voire même que c'était quelque chose dont
on se méfiait énormément pour la simple et bonne raison que j'ai appris une
expression il y a pas très longtemps "matérialisme dialectique"

j'ai compris que effectivement le droit et j'en conviens d'ailleurs est un outil qui
est créé par l'organe de répression d'Etat notamment pour assoir une répression et
et c'est vrai ; il n'y a pas de pas difficulté à le penser mais ça n'est pas
que ça et ça c'est aussi un outil et l'outil dans les lutes on se rend compte
de plus en plus que cet outil là peut être retourné que comme toute arme elle
peut être retournée contre l'oppresseur

et je voudrais rendre hommage à une
personne qui m'a beaucoup, beaucoup, beaucoup appris elle est là elle s'appelle
Geneviève Legay (https://fr.wikipedia.org/wiki/Affaire_Genevi%C3%A8ve_Legay)
et je voudrais d'abord qu'on l'appaudisse ; elle parlera demain
sur le plateau des Glières à 10h30 avec d'autres résistantes et
résistants oui Geneviève Legay est une grande grande résistante;  moi je l'ai rencontré
quand je quand j'ai entendu pour la première fois parler d'elle elle était
inconsciente puisque elle avait subi **une charge manifestement disproportionnée** ; la
justice le dira ; ordonnée par un commissaire divisionnaire, le commissaire Rabah Souchi,

à Nice elle était en train de manifester ; elle un gilet Jaune jaunid àé et etes revendications mais elle éta
aussi porte-parole d'attaque et donc les revendications contre néolibéralisme
autoritaiream et contre la régression en matière de droit soci etconomi mais la
dro de l'homme de bien d'autres organisation et de toutes les revendication toutes
les identités politiques et ce jour et bien ce jour-là elle est sur une place à
à à Nice et ce jour-là on a le lendemain le président chinois qui est censé
venir pour faire des accords économiques avec Emmanuel Macron il a rendez-vous
avec Emmanuel Macron et  donc le préfet des Alpes Maritimes interdit la
manifestation organisée ; une manifestation que tous savent particulièrement
pacifique parce que c'est toujours le cas à Nice ils sont pas beaucoup de
résistants à Nice mais ils sont toujours là et les policiers les connaissent
et ils sont systématiquement là en train de de discuter et de montrer qu'il y a
encore une résistance même dans les lieux où elle est plus difficile

et on a un commissaire qui va ordonner une charge ; d'abord il va ordonner cette charge à des
gendarmes et les gendarmes vont s'opposer à cette charge parce qu'ils disent mais
c'est pas possible c'est manifestement disproportionné, c'est un ordre illégal
et là on voit que surtout dans le corps de l'Etat, dans le corps de l'administration dans la
gendarmerie, dans la police il y a des résistances et il faut travailler avec ces
résistances et je pense bien évidemment à un autre résistant Amar Benmohamed (https://fr.wikipedia.org/wiki/Amar_Benmohamed)
brigadier chef de police au dépôt du palais de justice qui a dénoncé des
actes de maltraitance et de racisme dans le palais de justice de police de Paris
et qui et dans le palais de justice de Paris pardon et qui a a été harcelé
qui a subi des avertissements des blames et qui jusqu'à ce qu'on reconnaisse
sa qualité de lanceur d'alerte

juste vous dire que oui il y a des résistances dans toute l'épaisseur de
l'État et de l'administration et ça ce fut toujours le cas

Geneviève pour revenir à elle et bien elle a été chargée et elle
a failli en mourir ; on peut on peut le dire elle a eu des séquelles et elle a
encore beaucoup de séqueles et elle est encore là

le jour où ces filles m'ont appelé elles m'ont dit c'est pas possible, on doit
pouvoir obtenir justice et on a déposé plainte et ce jour-là :

- on a eu un procureur de la République de Nice qui a dit les policiers n'ont
  pas touché Gerneviève
- et puis on a eu un maire de Nice Christian Estrosi qui a dit les policiers
  n'ont pas touché Geneviève
- et puis on a eu é
- et puis on a eu Emmanuel Macron lui-même à Nice-mation qui a dit et qui
  a alors que elle était encore inconsciente qui a suggéré à Genevève Legay
  de rester chez elle à son âge parce que les manifestations c'est dangereux
  et qu'il vaut mieux ne pas y aller à son âge et qu'il y a une forme de
  sagesse ;

jusqu'à ce qu'on se rende compte notamment grâce à MediaPart  et à
Pascale Pascariello  une incroyable journaliste qui a fait un travail justement
au sein de ses résistances qui s'est rendue compte que avec le travail des
avocats également qui se sont qui sont intervenus et bien que le procureur
de la République de Nice avait menti ; lui-même a reconnu dans les colonnes
du monde plus tard qu'il avait menti pour protéger Emmanuel Macron
évidemment la raison d'État

in fine et bien on a eu une audience il y a pas
très longtemps à Lyon parce que l'affaire a été dépaysée c'est une des seules
manières d'obtenir justice puisque **la justice évidemment est structurellement
partiale s'agissant des violences policières**

on a été dépaysé et puis le commissaire Rabah Souchi en qualité de donneur
d'ordre et c'est ça la singularité quelque chose d'inédit **qui montre qu'on
avance malgré l'avancée des forces réactionnaires**, malgré les difficultés
qu'on connaît avec des femmes, comme des résistantes comme Genviève Legay
puisque'elle n'a jamais laché le combat , elle a toujours
été présente et bien on a réussi à faire condamner elle a réussi à faire
condamner le commissaire Rabah Souchi, **pour avoir donné des ordres de violence manifestement
disproportionnés**

juste pour vous dire que le droit et les résistances permettent des avancées
fondamentales et qui vont permettre notamment **cette jurisprudence va permettre
à d'autres victimes de violence policière d'obtenir justice**.

**ça c'est pour le droit**.


Liens
------

- https://france3-regions.francetvinfo.fr/provence-alpes-cote-d-azur/alpes-maritimes/nice/direct-affaire-legay-2e-jour-du-proces-du-commissaire-rabah-souchi-2905031.html


La manifestation contre l'antisémitisme du 12 novembre 2023 et la manifestation contre l'islamophobie le 10 novembre 2019
==========================================================================================================================

- https://youtu.be/bY3LOK2eMVU?t=453
- https://www.lemonde.fr/politique/article/2019/11/10/la-gauche-presque-au-complet-a-la-manifestation-contre-l-islamophobie_6018705_823448.html

Sur vos questions, je vais finir avec ; on devrait jamais finir avec les questions
qui fâchent **mais elles me fâchent personnellement**.

Ce que je veux dire c'est que je j'ai des dilemmes comme nous avons tous et
toutes des dilemmes ce ne sont pas des questions qui nous opposent uniquement
ce sont des questions qui créent des fractures en nous-mêmes dans des moments
de crise tel que celui que nous vivons
Il y en a eu d'autres et il y en aura d'autres évidemment et donc il faut
arriver à penser ces moments de crise, de dilemme intérieur et de dilemme des
forces politiques et sociaux de manière générale.

Comment est-ce qu'on arrive à penser ces moments, comment par exemple avant même
de parler de la manifestation du 12 novembre contre l'antisémitisme qui a fracturé
le positionnement des gauches ;

certains ont dit qu'il ne fallait pas y aller d'autres ont dit qu'il faillait y aller
absolument

Comment est-ce qu'on a pensé ces moments et comment est-ce qu'on doit prendre
une position c'est extrêmement compliqué évidemment mais moi je pense à une
autre manifestation : `10 novembre 2019 <https://www.lemonde.fr/politique/article/2019/11/10/la-gauche-presque-au-complet-a-la-manifestation-contre-l-islamophobie_6018705_823448.html>`_ sauf erreur de ma part c'était la
manifestation contre l'islamophobie à un moment où les musulmans de France
étaient visés notamment par l'État que après ce que nous avons avions vécu
pendant l'état d'urgence où des familles musulmanes avaient été spécifiquement
visées;  où il y avait une doctrine qui se développait au sein de l'État qui
consistait à créer une continuité entre l'islam, l'islamisme et le terrorisme
et évidemment avec ces chaînes dont Agnès parlait tout à l'heure comme Cnews
qui diffusaient des idéologies d'extrême droite et islamophobes et qui tapaient
en permanence sur les musulmans.

Qu'est-ce qu'on fait quand il y a une manifestation qui est organisée contre
l'islamophobie après je le rappelle un attentat contre la mosquée de Bayonne ?
est-ce qu'on y va ou est-ce qu'on y va pas ?

La même question s'est posée le 12 novembre 2023 est-ce qu'on y va ou est-ce
qu'on n'y va pas ?

Comment on fait résonner nos différentes identités politiques, professionnelles,
culturelles ?

Je crois que la seule manière de raisonner dans ces moments-là c'est et pour
ne pas nous diviser, pour ne pas diviser les forces, pour ne pas diviser la société
qui a besoin de se reserrer je crois qu'il ne faut pas diviser nos principes
et c'est peut-être une équation pour ne pas nous diviser, ne pas diviser nos principaux
articuler nos différents principaux.

La lutte contre le racisme, tous les racismes, pas un racisme contre un autre
la lutte pour la vie et pour la protection de la vie notamment sur les violences
policières, notamment le 7 octobre ; ne pas oublier la protection pour la vie,
la lutte pour l'égalité, pour l'indépendance pour l'autodétermination des peuples
également

Tous ces principes là ne doivent pas s'opposer c'est la seule manière que nous
avons de ne pas nous diviser.

Alors quand il y a des moment comme la manifestation contre l'islamophobie
et que certains se posent la question de savoir si on y va pourquoi
parce que on nous dit et les forces médiatiques réactionnaires disent oui mais
il y a des islamistes on peut pas manifester à côtés d'islamistes qui sont contre
le féminisme qui sont contre l'égalité, qui veulent imposer un régime religieux
en France avec ce fameux Grand remplacement fictif qui crée une guerre de civilisation

Qu'est-ce qu'on fait ? on accepte ce dicktat ?
On accepte cet endoctrinement réactionnaire ? ou bien on se met à côté des
musulmans on va manifester avec eux parce qu'on milite contre l'islamophobie,
contre toutes les atteintes et l'essentialisation et la stigmatisation de nos
frères et nos sœurs.

Voilà ce que vous fait et oui on y va et on l'affirme même et même si on se
prend des foudres derrière et on s'en est pris des foudres.

Et ben c'est pareil pour le 12 novembre dernier (2023) on a nos frères et
nos sœurs de confession juive qui ressentent une douleur profonde, une peur avec
une augmentation de l'antisémitisme en France alors certes il y a plein de raisons de
pas y aller; évidemment ceux qui ont organisé la manifestation ont une arrière
pensée ; une instrumentalisation politique de l'antisémitisme éventuellement pour
soutenir Israël ;

Bien sûr qu'ils ont cette arrière pensée comme le 10 novembre 2019 certains
avaient une arrière pensée ; évidemment on a cette arrière pensée et
puis on a aussi l'extrême droite qui vient comme il pouvait y avoir des islamistes
avec qui on n'est pas forcément copain le 10 novembre 2019 mais c'est pas avec eux qu'on
manifeste c'est pour ceux qui sont visés par l'antisémitisme et par le racisme
et par l'islamophobie et c'est à ce moment-là qu'on pense ensemble et qu'on ne
divise pas la lutte contre les racismes et contre l'antisémitisme comme on ne
divise par toutes les luttes.

[applaudissements]

je n'au plus rien d'autre à dire.

Quelques photos
====================

.. figure:: images/20240511_100156_800_arie_alimi.webp
.. figure:: images/20240511_102507_800_les_4.webp
.. figure:: images/20240511_122032_800_dedicaes_alimi.webp
.. figure:: images/20240511_132411_800_dedicaces_librairie.webp
.. figure:: images/20240511_133617_800_arie_gilles.webp
.. figure:: images/20240511_195041_800_reza_israel_palestine.webp


Sur les réseaux sociaux
==========================

