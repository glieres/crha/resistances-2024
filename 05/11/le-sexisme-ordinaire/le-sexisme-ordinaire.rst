.. index::
   ! Atelier participatif **Le sexisme ordinaire** avec La Mêlée **de 10h à 12h30**
   ! La Mêlée

.. _melee_2024_05_11:

===============================================================================================================================================================
2024-05-11 Atelier participatif **Le sexisme ordinaire** avec La Mêlée **de 10h à 12h30**
===============================================================================================================================================================

- https://www.citoyens-resistants.fr/Documents/XR.pdf
- https://extinctionrebellion.fr/branches/annecy/

Présentation
===============

Atelier participatif organisé par La Mêlée, association d'éducation populaire aux
féminismes.

Le sexisme ordinaire, c'est à dire ?
-----------------------------------------

On appelle sexisme ordinaire les pratiques et stéréotypes sexistes présents
dans la vie quotidienne et omniprésents dans nos sociétés, mais assez discrets
pour qu'on ne les remarque plus spécialement.

L'accumulation de ces petits actes, sans violence évidente lorsqu'on les prend
individuellement, crée pourtant de la violence par leur répétition et contribue à
perpétuer les stéréotypes de genre et la domination patriarcale.

Le but de cet atelier en intelligence collective sera d'échanger sur le sexisme
ordinaire et d'envisager des solutions pour lutter contre ce dernier.

Cet atelier sera mixte.

Nous limitons le nombre de participant.es.x à 25 personnes.

Merci de vous inscrire par mail à associationlamelee@protonmail.com.

Animatrices
=============

Cloé et Alice, de La Mêlée.

Animation
===========

Les ateliers sont animés par des membres de l’association XR d’Annecy.
