.. index::
   pair: Conférence ; Rendez nous l'espoir ! (2024-05-11)
   ! Rendez nous l'espoir

.. _ruffin_2024_05_11:

=========================================================================================================================
2024-05-11 Conférence **Rendez nous l'espoir !** salle Tom Morel avec François Ruffin et Gilles Perret **de 17 à 19h**
=========================================================================================================================

- http://www.citoyens-resistants.fr/Documents/Programme2024.pdf
- http://citoyens-resistants.fr/Documents/Rendez-nous%20l%20espoir-%20texte%20mis%20en%20forme%20V2.pdf

.. figure:: images/affiche.webp

Présentation
===============

Mars 1944
-------------

Le Conseil National de la Résistance (CNR) annonce un programme intitulé
"Les Jours Heureux », socle de notre modèle social français et des valeurs
de CRHA.

Avril 2024
-------------

Comme Sarkozy en 2007, Macron vient parader aux Glières pour rendre hommage
aux valeureux combattants des Glières, mais n’a aucun mot pour le Programme du CNR
qu’ils n’ont eu de cesse de détricoter et de piétiner.

Le bilan de ces 80 dernières années est dur à avaler :

- casse des services publics,
- démantèlement des conquis sociaux (droits du travail, Sécurité sociale, retraite,
  assurance chômage...)
- auquel s’ajoute une grave atteinte aux libertés publiques et une criminalisation
  des militant.e.s sociaux et écologistes, ce qui a un effet sidérant
  sur les militant.e.s qui luttent pour une société solidaire.

Partant de ce constat, cette conférence se veut un cri : il nous faut retrouver
l’espoir !

Les intervenant.e.s et la salle tenteront d’apporter des pistes pour un avenir
désirable, parce que "À la Fin, c’est Nous qu’on DOIT Gagner ! »


Les intervenant.e.s et co-animateur.rice.s
===============================================

- François RUFFIN, journaliste créateur de Fakir, réalisateur et député
- Gilles PERRET, co-créateur et membre de CRHA, réalisateur
- une troisième intervenante encore en cours de recherche


Liens
========

Face à l’extrême droite : refaire la gauche et son imaginaire, radicalement par Philippe Corcuff
----------------------------------------------------------------------------------------------------

- :ref:`raar_2024:corcuff_2024_04_29`
