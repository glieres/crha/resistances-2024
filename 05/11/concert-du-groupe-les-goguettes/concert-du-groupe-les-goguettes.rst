.. index::
   ! Concert du groupe **les Goguettes** salle Tom Morel à 20h

.. _goguettes:

==========================================================================
2024-05-10 🎉 Concert du groupe **les Goguettes** salle Tom Morel à 20h
==========================================================================

- https://www.citoyens-resistants.fr/
- https://www.citoyens-resistants.fr/Documents/Goguettes.pdf


.. figure:: images/affiche.webp
