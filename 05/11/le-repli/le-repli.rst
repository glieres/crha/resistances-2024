.. index::
   pair: Analyse ; montée des discours racistes en France depuis le début des années 80 et la restriction des libertés depuis 2015
   ! Le repli (en avant première)

.. _paris_2024_05_11:
.. _repli_2024_05_11:

==========================================================================================================
2024-05-11  🎥 **Le repli** en avant première de  Joseph Paris au cinéma Le Parnal à **10h30**
==========================================================================================================

- http://www.citoyens-resistants.fr/Documents/Programme2024.pdf
- https://josephparis.fr/le-repli/


Résumé: analyse la montée des discours racistes en France depuis le début des années 80 et la restriction des libertés depuis 2015
=====================================================================================================================================

Le Repli **analyse la montée des discours racistes en France depuis le début
des années 80 et la restriction des libertés depuis 2015**.


Le repli
===============================

Yasser, militant des droits humains, parcourt Paris en tous sens, en voiture,
à pieds, en transport, pour délivrer une parole qu’on ne veut pas entendre.

Joseph, réalisateur, le suit et prend en filature le phénomène du repli identitaire
en France, décortiquant le discours médiatique et politique sur cinq décennies.

**Confrontant l’actualité à l’archive, Joseph et Yasser cherchent à comprendre
pourquoi nos libertés ne cessent de reculer depuis plusieurs années.**

Conférence en lien : Main basse sur les libertés publiques à 10h
===================================================================

- :ref:`alimi_2024_05_11`
