.. index::
   pair: Conférence ; Le sol notre avenir salle Tom Morel avec Alberte BONDEAU, Julie TROTTIER et Raphaël BALTASSAT **de 14h à 16h
   ! Alberte BONDEAU
   ! Julie TROTTIER
   ! Raphaël BALTASSAT

.. _trottier_2024_05_11:

==============================================================================================================================================================
2024-05-11 Conférence **Le sol notre avenir** salle Tom Morel avec Alberte BONDEAU, Julie TROTTIER et Raphaël BALTASSAT **de 14h à 16h**
==============================================================================================================================================================

- http://www.citoyens-resistants.fr/Documents/Programme2024.pdf
- http://citoyens-resistants.fr/Documents/Conference_2024.pdf

.. figure:: images/affiche.webp


Sous-titre ou explication
============================

Le sol n’est pas que cette surface plus ou moins plate sur laquelle nous marchons
et vivons.

À l’état naturel il a, sous sa surface, un fonctionnement très intense et complexe
sans lequel aucune vie ne peut se développer sur terre.

Cette conférence tentera donc de répondre à 4 questions sur cette vie du sol:

- Comment fonctionne un sol vivant et en quoi il est indispensable aux
  vivants sur Terre ?
- Qu'est-ce qui le détruit et à quelles catastrophes sa destruction conduit-elle ?
- Quelles conséquences de cette destruction sur les eaux, elles aussi indispensables
  à la vie sur terre ?
- Comment régénérer des sols détruits mais aussi comment produire en profitant
  de ses richesses et sans le détruire ?

Courte présentation des invité·e·s pour **Le sol**
=====================================================

- Alberte BONDEAU, chargée de recherche CNRS à l’IMBE de Marseille
- Julie TROTTIER, directrice de recherche CNRS à l’unité PRODIG à Paris
- Raphaël BALTASSAT, paysan en haute-Savoie, membre de la Confédération
  Paysanne

Présentation plus longue
===========================

- Alberte BONDEAU, chargée de recherche au CNRS, travaille à l’IMBE (Institut
  Méditerranéen de Biodiversité et d’Ecologie marine et continentale) à Marseille,
  sur la modélisation des interactions agriculture-climat-biodiversité dans le but de
  mieux comprendre les clés de la durabilité des agro-écosytèmes.
- Julie TROTTIER, directrice de recherche CNRS à l’unité PRODIG (Pôle de
  Recherche pour l’Organisation et la Diffusion de l’Information Géographique),
  travaille depuis 30 ans sur la construction des discours scientifiques concernant
  l’eau et les choix technologiques la concernant.
- Raphaël BALTASSAT, paysan en Hte-Savoie, producteur de lait à reblochon et de
  farines panifiables en AB, membre de la Confédération Paysanne

Animation
-------------

- Mireille BERNEX, membre de CRHA


