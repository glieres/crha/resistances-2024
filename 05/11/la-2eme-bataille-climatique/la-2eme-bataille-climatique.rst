.. index::
   pair: Conférence ; La 2ème bataille climatique** sous le chapiteau avec Dominique Bourg, Fréderic LE MANACH et Valérie PAUMIER **de 10h30 à 12h30

.. _bourg_2024_05_11:

==============================================================================================================================================================
2024-05-11 Conférence **La 2ème bataille climatique** sous le chapiteau avec Dominique Bourg, Fréderic LE MANACH et Valérie PAUMIER **de 10h30 à 12h30**
==============================================================================================================================================================

- http://www.citoyens-resistants.fr/Documents/Programme2024.pdf
- http://citoyens-resistants.fr/Documents/Conference_2024.pdf
- http://www.youtube.com/watch?v=FHEIyTv4SXI

.. figure:: images/affiche.webp


Description
============

Face à la catastrophe écologique où nous a menés le capitalisme, la première
bataille du climat est perdue.

Parce qu'elle est liée au développement capitaliste, la dévastation de la
planète réclame une vraie politique de rupture.

Bien autre chose que le greenwashing et tous agissements qui occultent la réalité.

Aucun n'a considéré avec sérieux la gravité du dérèglement climatique, encore
moins tenté d'’en inverser le cours.

Pour qu'enfin la transformation s'engage, nous devons inventer un nouvel
art politique : **une vraie démocratie écologique**.

Nous n'avons pas d'autre choix. Mais ce défi est colossal.

Pour en débattre nous avons la chance de pouvoir réunir trois acteurs engagés
dans ce combat.

Ils ont accepté de venir témoigner et débattre de leurs travaux et de leur
engagement.

Les intervenant·e·s
======================

- Dominique BOURG, philosophe et professeur honoraire en sciences de
  l'environnement à l'université de Lausanne est un défenseur passionné de
  l'Ecologie Politique
- Fréderic LE MANACH, docteur en gestion des pêches est directeur scientifique de
  l'association BLOOM, ONG qui milite pour la protection des océans et pour la
  pêche artisanale.
- Valérie PAUMIER, Présidente de l'association Résilience Montagne, véritable
  sentinelle du climat, nous alerte sans cesse sur les dégâts trop souvent provoqués
  par les activités touristiques dans les montagne

Animateur
-------------

- Gérard DECORPS, membre de CRHA


Liens concernant l'imaginaire
======================================

Face à l’extrême droite : refaire la gauche et son imaginaire, radicalement par Philippe Corcuff
--------------------------------------------------------------------------------------------------

- :ref:`raar_2024:corcuff_2024_04_29`

Samedi 11 Mai, au forum des résistant.e.s : La deuxième Bataille Climatique avec @bourg_d, philosophe et professeur Frédéric Le Manach, directeur scientifique de @Bloom_FR @Valerie_Paumier, présidente de l'association Résilience Montagne #educationpopulaire #climat
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- https://nitter.poast.org/CRHA_glieres/status/1790805632888545635#m
