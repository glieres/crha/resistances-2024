.. index::
   ! Le féminisme est-il soluble dans le capitalisme ?** sous le chapiteau de 14h à 16h avec Sandrine Holin, Suzy Rojtman et Fanette Freydier

.. _holin_2024_05_11:

===============================================================================================================================================================
2024-05-11 ♀️ **Le féminisme est-il soluble dans le capitalisme ?** sous le chapiteau avec Sandrine Holin, Suzy Rojtman et Fanette Freydier **de 14h à 16h**
===============================================================================================================================================================

- http://citoyens-resistants.fr/Feminismecapitalisme.pdf

Présentation
===============

Il semblerait que le féminisme se fasse rattraper par le capitalisme :

Après le greenwashing, la récupération de l'écologie par le capitalisme,
verrait-on poindre le purplewashing, la récupération des mouvements féministes
par ce même capitalisme néolibéral ?

Comment les luttes féministes sont utilisées par le capitalisme néo libéral,
pour son propre profit, pour servir ses propres intérêts ?

Auraient-elles quelque chose à gagner dans ces nouvelles promesses d'égalité
empreintes de compétitivité et d'individualisme ?

Le capitalisme n'est-il pas structurellement patriarcal ?

**Et si le féminisme était au contraire le levier du changement ?**


Intervenantes
=====================================================

- Sandrine Holin, autrice de l’essai "Chères collaboratrices" .
- Suzy Rojtman, porte-parole du Collectif National pour les Droits des Femmes.
  Cofondatrice du Collectif féministe contre le viol en 1985.

- Animée par Fanette Freydier, compagnonne de CRHA

