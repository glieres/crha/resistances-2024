.. index::
   pair: librinfo74; Contre l’extrême droite, tous aux Glières ce week end
   pair: Philippe Corcuff ; Contre l’extrême droite

.. _librinfo_2024_05_06:
.. _librinfo74_2024_05_06:

========================================================================================
2024-05-06 **Contre l’extrême droite, tous aux Glières ce week end** par librinfo74
========================================================================================

- https://librinfo74.fr/contre-lextreme-droite-tous-aux-glieres-ce-week-end/
- https://www.citoyens-resistants.fr/
- https://www.librairie-rousseau.com/
- http://citoyens-resistants.fr/associations.html
- :ref:`librinfo74`


Contre l’extrême droite, tous aux Glières ce week end
=========================================================

Librinfo sera présent le :ref:`samedi 11 mai 2024 à Thorens <2024_05_11>`.
Vous pourrez nous rencontrer sur notre stand.

Nous serons également présent :ref:`le dimanche 12 mai 2024 <2024_05_12>`
sur le plateau des Glières avec l’intervention de notre journaliste Gérard Fumex,
victime d’une agression par les militants d’extrême droite de Reconquête
le 26 novembre 2023 pendant son travail de journaliste
(voir article dans librinfo : https://librinfo74.fr/gerard-fumex-journaliste-a-librinfo-a-ete-agresse-sauvagement-par-des-sbires-de-lextreme-droite/)

- Le rassemblement débutera le vendredi 10 mai à 20h00 avec :ref:`la pièce de théâtre
  «Discours de la servitude volontaire» <servitude_2024_05_10>`
  d’Etienne de la Boétie traduit en langage moderne par la  Compagnie Avec vue sur la mer.
  Pas de réservation.


Samedi 11 mai 2024
======================

Le samedi 11 mai 2024, ce sont:

- `les conférences http://citoyens-resistants.fr/Documents/Conference_2024.pdf <http://citoyens-resistants.fr/Documents/Conference_2024.pdf>`_,
- les films,
- :ref:`les expositions <expositions_libreinfos>`,
- la `librairie scop Jean Jacques Rousseau de Chambéry <https://www.librairie-rousseau.com/>`_,
- et bien sûr notre Village des associations (en savoir plus `ici <http://citoyens-resistants.fr/associations.html>`_), mais aussi le soleil, le ciel bleu…

Pour en savoir plus sur les conférences notamment les intervenants,  vous pouvez
voir toutes les fiches ici, ou cliquez sur un titre pour en voir une seule.

Deux conférences seront traduites en Langue des Signes Française (LSF).

Conférences
=============

- http://citoyens-resistants.fr/Documents/Conference_2024.pdf

- :ref:`10h00 "Main basse sur les libertés publiques" <alimi_2024_05_11>`
- :ref:`10h30 "La deuxième bataille climatique" <bourg_2024_05_11>`
- :ref:`14h00 "Le sol, notre avenir" (LSF) <trottier_2024_05_11>`
- :ref:`14h00 "Le Féminisme est-il soluble dans le capitalisme" <holin_2024_05_11>`
- 14h00 "Dans quel monde vivons nous, regards géopolitiques et actions politiques ? annulée»
- Remplacée par l’atelier: Grands projets inutiles imposés, les ingrédients de la victoire.
- :ref:`17h00 "Pour une sécurité sociale alimentaire ?» <sec_so_2024_05_11>`
- :ref:`17h00 "Rendez nous l’espoir" (LSF) <ruffin_2024_05_11>`
- :ref:`17h00 "Etat républicain, conseils ouvriers et municipalisme libertaire, quelles perspectives ?" <etat_2024_05_07>`


.. _expositions_libreinfos:

Expositions
===============

- :ref:`Regards croisés, Plantu, Reza <reza_2024_05>`
- :ref:`les Robes Grises <robes_grises>`


Les Films prévus
=====================

Vendredi 10 mai 2024
------------------------------

- `17h00, Dounia et la princesse d’Alep <https://www.youtube.com/watch?v=axyj1Zwpo1k>`_: de Marya Zarif et André Kadi
- 20h00, (2024 / 2h20 ) :ref:`Guérilla des FARC, l’avenir a une histoire <farc_2024_05_10>` de Pierre Carles, en avant-première

Samedi 11 mai 2024
=========================

Films en présence des réalisatrices et réalisateurs
-------------------------------------------------------

- 10h30, (2024 / 1h30) :ref:`Le repli de Joseph Paris en avant-première <repli_2024_05_11>`
- 13h30, (2024 / 1h20) :ref:`Nous n’avons pas peur des ruines de Yannis Youlountas <youlountas_2024_05_11>`
- 16h00, (2019 / 52mn) :ref:`Le fantôme de Theresienstadt de Baptiste Cogitore <cogitore_2024_05_11>`
- 17h30, (2023 / 1h30) :ref:`Le balai libéré de Coline Grando <balai_libere_2024_05_11>`
- 20h30, :ref:`Gaza, sélection de courts métrages <alasttal_2024_05_11>` de Iyad Alasttal


Ateliers, formations
-------------------------

- :ref:`10h/12h et 14h/16h : Formation "anti répression" avec Extinction Rebellion <xr_2024_05_11>`
- :ref:`10h/12h30 : Sexisme ordinaire avec La Mêlée <melee_2024_05_11>`
- 14h/15h30 : Grands projets inutiles imposés, les ingrédients de la victoire.
- Des échanges seront organisés autour du SNU.


Samedi 11 mai 2024 à 20h00, concert du groupe "les Goguettes".
-------------------------------------------------------------------

- :ref:`concert du groupe "les Goguettes" <goguettes>`.
  Pas de réservation.


Dimanche 12 mai 2024 à 10h30, ce sont les Paroles de résistances sur le Plateau des Glières
===============================================================================================

Toutes les interventions sont traduites LSF.

- Giorgos Kalaitzidis, militant attaqué par l’état grec
- Hommage à Charles Piaget
- Mélanie Berger-Volle, Autrichienne résistante en France, lecture de son texte
- Iyad Alasttal, réalisateur gazaoui
- Geneviève Legay, manifestante blessée par une charge de police
- Gérard Fumex, journaliste, agressé par l’extrême droite
- Thomas Brail, écureuil du Tarn
- Céline Verzeletti, syndicaliste, citée à comparaitre au tribunal

Mais aussi des films
--------------------------

- 15h00, (2024 / 1h40) `Etat limite <https://www.alchimistesfilms.com/etat-limite>`_ de Nicolas Peduzzi
- 17h30, (2013 / 1h40) `Les jours heureux <https://www.youtube.com/watch?v=EV--23S97zM>`_ de Gilles Perret


Liens concernant l'extrême droite
======================================

Face à l’extrême droite : refaire la gauche et son imaginaire, radicalement par Philippe Corcuff
--------------------------------------------------------------------------------------------------

- :ref:`gauche`
