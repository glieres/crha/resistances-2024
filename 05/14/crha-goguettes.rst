
.. _crha_2024_05_14:

===============================================================================
2024-05-14 🎼 **Quelle soirée inoubliable avec les #Goguettes**
===============================================================================

-https://x.com/CRHA_glieres/status/1790333461267349826

🎼 Samedi 11 Mai, quelle soirée inoubliable avec les #Goguettes, en trio mais à quatre.
Des éclats de rire, une ambiance festive, des parodies, quel spectacle !!
Retour en images ⤵️

#CRHA #spectacle #concert #musique #culture
