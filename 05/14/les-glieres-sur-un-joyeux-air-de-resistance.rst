
.. _librinfo74_2024_05_14:

===============================================================================
2024-05-14 **Les Glières sur un joyeux air de résistance** par librinfo74
===============================================================================

- https://librinfo74.fr/les-glieres-sur-un-joyeux-air-de-resistance/
- :ref:`librinfo74`


Préambule
=============

**En ce long weekend de l’Ascension, le Rassemblement des Glières a pris place
comme chaque année de vendredi à dimanche soir à Fillière et sur le Plateau
des Glières. Pour sa 18e édition, celui-ci a fait largement salle comble,
offrant la parole à toute une kyrielle de résistant.e.s à l’air du temps,
cherchant à redonner espoir, en paroles et en musique, aux quelques 2500 personnes
ayant fait le déplacement dans la petite commune de Haute-Savoie** :

L’association CRHA est née à la suite des rassemblements citoyens qui
s’étaient organisés en 2007 et 2008 en riposte à l’époque à la tentative
de récupération du lieu historique des Glières et de l’héritage de la
Résistance par Nicolas Sarkozy.

Elle fut parrainée dès son lancement par les anciens résistants Stéphane Hessel,
Raymond Aubrac et Henri Bouvier et organise depuis lors à chaque printemps
son rassemblement, attirant en moyenne entre 2000 et 3000 personnes le temps
d’un weekend sur la commune de Thorens-Glières et sur le plateau des Glières.

Ce 10 mai 2024 après-midi, la météo est au beau fixe alors que s’activent les
dizaines de bénévoles de l’association :ref:`CRHA <[1]_camille_2024_05_14>` autour de la salle Tom Morel afin
que tout soit prêt pour l’ouverture de l’édition 2024 du Rassemblement des
Glières.

Celui-ci débute dès 17h au petit cinéma du Parnal avec la projection du
film d’animation Dounia et la :ref:`Princesse d’Alep <[2]_camille_2024_05_14>` suivi à 20h du dernier film
de Pierre Carles en avant-première et en présence de celui-ci.

Théâtre, cinéma, conférence, ateliers, exposition photos… la cuvée 2024 de
ce rassemblement offre à nouveau un programme très dense et diversifié, avec
son Forum des résistances le samedi dans le centre de la commune de
Thorens-Glières et ses paroles de résistances le dimanche en fin de matinée
sur le Plateau des Glières.

Résister, un thème d’actualité
=================================

Guerre à Gaza, guerre en Ukraine, répression accrue des opposant.e.s un
peu partout sur la planète, comme en Iran, mais également en France où les
pouvoirs publics réduisent de plus en plus le champ des libertés publiques
sur fond d’ascension des idées d’extrême droite et d’accélération du
dérèglement climatique…

Dans un contexte national et international de plus en plus anxiogène, le thème
des résistances d’hier et d’aujourd’hui porté par le rassemblement des Glières
semble on ne peut plus d’actualité.

La première grande conférence de ce samedi matin, "main basse sur les libertés
publiques", annonce la couleur, avec le célèbre journaliste Edwy Plenel, tout
juste retiré de la direction de Médiapart, la directrice de l’hebdomadaire
Politis Agnès Rousseaux et l’avocat Arié Alimi.

La salle Tom Morel a du mal à contenir les plus de 500 personnes venues les
écouter décrire l’évolution inquiétante des libertés publiques, de la liberté
d‘expression et de la liberté de la presse en France, sur fond de répression
policière et judiciaire de plus en plus forte.

La résistance écologique n’est pas en reste, l’autre conférence de la
matinée étant consacrée à la "bataille climatique", avec entre autres
le célèbre philosophe Dominique Bourg.

Celle-ci se tient à une demi-heure d’intervalle sous le chapiteau dressé pour
l’occasion devant l’école publique et la MJC de Thorens, à l’ombre du vieux
:ref:`chêne pédonculé, un des plus grands de Haute-Savoie <[3]_camille_2024_05_14>`.

Certain.es apprennent pendant ce temps à résister concrètement lors d’ateliers
participatifs, contre le sexisme ordinaire, à l’intérieur de la MJC, avec
:ref:`La Mêlée <[4]_camille_2024_05_14>` ou pour apprendre les comportements utiles en cas de garde à vue,
avec Extinction Rebellion Annecy à l’espace Forestier.

En parallèle, le Parnal propose également tout au long de la journée des
films suivis de rencontres avec leur réalisateur, abordant la résistance sous
toutes ses formes, contre le repli identitaire en France (:ref:`Le Repli <[5]_camille_2024_05_14>`), contre
la casse néolibérale en Grèce (:ref:`Nous n’avons pas peur des ruines <[6]_camille_2024_05_14>`), contre
l’horreur des camps nazis en 1942 (:ref:`Le fantôme de Theresienstadt <[7]_camille_2024_05_14>`) ou encore
contre la guerre à Gaza avec une sélection de courts métrages du réalisateur
:ref:`palestinien réfugié en France Iyad Alastall <[8]_camille_2024_05_14>`.

Espérer, une nécessité
============================

Certes dans le contexte actuel, résister c’est bien, mais pouvoir espérer un
peu et proposer, c’est mieux.

Et c’est ce que cherche à faire notamment la principale conférence de ce
samedi après-midi, intitulée Rendez-nous l’espoir, en présence notamment du
très médiatique député-reporter François Ruffin, compagnon de route de toujours
du rassemblement des Glières et proche du réalisateur Gilles Perret, pilier
de CRHA et avec qui il a réalisé des documentaires comme :ref:`J’veux du soleil <[9]_camille_2024_05_14>`
ou :ref:`Debout les femmes <[10]_camille_2024_05_14>`.

Durant une trentaine de minutes et dans une salle Tom Morel débordant largement,
environ 700 personnes écoutent le député de la Somme citer notamment Ambroize Croizat,
le père de la Sécurité sociale : "S’unir, plus que jamais s’unir, pour
donner à la France d’autres espoirs."

En parallèle, sous le chapiteau comme dans la MJC, des tables rondes débattent
de propositions comme :ref:`la sécurité sociale alimentaire <[11]_camille_2024_05_14>`
et d’outils issus du féminisme ou des conseils ouvriers et du municipalisme libertaire,
toujours en présence d’expert.e.s, militant.e.s, syndicalistes ou élu.e.s impliquées sur
toutes ces questions (Alice Desbiolle, Anne Souyris, Marie Ducruet, Suzy Rojtman,
Yannis Youlountas, Benjamin Sèze ou encore Daniel Ibanez).

Les paroles de résistance du dimanche, en haut du plateau des Glières, sont
aussi tournées vers l’espoir, avec des témoignages inspirants, comme celui
de Geneviève Legay, militante infatigable malgré les violences policières, de
Thomas Brail, qui se bat efficacement et de tout son être pour sauvegarder les
arbres, de Gérard Fumex, notre journaliste local de Librinfo qui s’était fait
agresser par un nervi d’extrême droite à Annecy en marge d’une manifestation
en novembre dernier, de Céline Verzeletti, syndicaliste qui refuse de se taire
sur l’horreur actuelle à Gaza malgré les pressions judiciaires ou encore
l’émouvant hommage rendu par sa fille à Charles Piaget, syndicaliste de Lipp
décédé en novembre dernier.


Devoir de joie
===============

Si résister et espérer sont nécessaires par les temps qui courent, encore
faut-il ne pas oublier de prendre également un peu de plaisir et de s’amuser.

Et à ce propos le point d’orgue de ce weekend fut sans conteste le :ref:`concert des
Goguettes <[12]_camille_2024_05_14>`, le samedi soir dans la salle Tom Morel.

Le "trio mais à quatre", qui s’est fait connaître du grand public par ses parodies particulièrement
inspirées de chansons françaises pendant le confinement, s‘en est donné à
cœur joie pendant près de deux heures, réjouissant petits et grands dans un
style et une esthétique surannés de cabaret, au seul son du piano et des voix
bariolées des quatre compères, et décochant au passage quelques flèches bien
senties sur nos dirigeants.

Les sourires n’ont pas manqué durant tout ce rassemblement ensoleillé,
croisés sur les milliers de visages, et même sur les stands des partis
politiques présents, à pourtant un mois des élections européennes.

Comme quoi le rassemblement est peut-être avant tout une question d’ambiance, loin des
plateaux télés et des réseaux sociaux aux débats hystérisés.

Et il faut bien garder en tête, comme le résume cette citation généralement
accordée à Boris Vian, que "l’humour est la politesse du désespoir"

Les principales interventions du weekend sont disponibles sur LibRadio :
https://libradio.org/11-12-mai-2024-rassemblement-des-glieres-2024/


Notes
=========

.. _[1]_camille_2024_05_14:

[1] CRHA : Pour Citoyen.nes Résistant.es d’Hier et d’Aujourd’hui, voir https://www.citoyens-resistants.fr/
------------------------------------------------------------------------------------------------------------

.. _[2]_camille_2024_05_14:

[2] Voir https://www.hautetcourt.com/animation/dounia-et-la-princesse-dalep/
--------------------------------------------------------------------------------

.. _[3]_camille_2024_05_14:

[3] Chêne pédonculé, un des plus grands de Haute-Savoie : https://www.monumentaltrees.com/fr/fra/hautesavoie/thorensglieres/26172_cimetire/
--------------------------------------------------------------------------------------------------------------------------------------------

.. _[4]_camille_2024_05_14:

[4] La Mêlée: https://www.helloasso.com/associations/la-melee-association-d-education-populaire-aux-feminismes
------------------------------------------------------------------------------------------------------------------

.. _[5]_camille_2024_05_14:

[5] Le Repli: https://www.film-documentaire.fr/4DACTION/w_fiche_film/68872
---------------------------------------------------------------------------

.. _[6]_camille_2024_05_14:

[6] Nous n’avons pas peur des ruines: https://www.film-documentaire.fr/4DACTION/w_fiche_film/70670_2
------------------------------------------------------------------------------------------------------------

.. _[7]_camille_2024_05_14:

[7] Le fantôme de Theresienstadt: Voir https://www.film-documentaire.fr/4DACTION/w_fiche_film/56247_0
-----------------------------------------------------------------------------------------------------------

.. _[8]_camille_2024_05_14:

[8] Iyad Alastall : https://france3-regions.francetvinfo.fr/corse/guerre-israel-hamas-la-vie-est-detruite-a-gaza-mais-j-espere-retourner-en-palestine-il-y-a-ma-famille-et-mes-souvenirs-2960477.html
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

.. _[9]_camille_2024_05_14:

[9] Voir https://jour2fete.com/film/jveux-du-soleil-2/
------------------------------------------------------------

.. _[10]_camille_2024_05_14:

[10] Voir https://jour2fete.com/film/debout-les-femmes/
------------------------------------------------------------

.. _[11]_camille_2024_05_14:

[11] Lire https://securite-sociale-alimentation.org/
-----------------------------------------------------------

.. _[12]_camille_2024_05_14:

[12] Voir https://www.lesgoguettes.fr/
-----------------------------------------
