

.. _compte_2024_05_19:

============================================================
2024-05-19 Compte-rendus, vidéos du CRHA
============================================================

- https://www.youtube.com/@CRHA_RassemblementdesGlieres


Les Paroles des Résistant.e.s d'Hier et Aujourd'hui ont résonné, écoutez leurs témoignages
===============================================================================================

- https://x.com/CRHA_glieres/status/1792193216781136070

- https://www.youtube.com/watch?v=ZV0wccec1tE

.. youtube:: ZV0wccec1tE

Les Paroles des Résistant.e.s d'Hier et Aujourd'hui ont résonné, écoutez leurs témoignages.
Toutes les paroles sont signées en #LSF.

Vibrant d'émotions, de courage et d'espoirs, 18 ans que #CRHA porte les valeurs du #cnr

Vibrant d'émotions, de courage et d'espoirs, 18 ans que #CRHA porte les valeurs du #cnr
==============================================================================================

- https://x.com/CRHA_glieres/status/1792194803519606968

Les Paroles des Résistant.e.s d'Hier et Aujourd'hui ont résonné, écoutez leurs témoignages.
Toutes les paroles sont signées en #LSF.

Vibrant d'émotions, de courage et d'espoirs, 18 ans que #CRHA porte les valeurs du #cnr

